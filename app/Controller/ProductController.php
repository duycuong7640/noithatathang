<?php

/**
 * Description of NewsController
 * @author : Yeu tinh
 * @since Oct 19, 2012
 */

class ProductController extends AppController
{

    public $name = 'Product';
    public $uses = array('Catproduct', 'Post', 'Product', 'Video');

    public function beforeFilter()
    {
        parent::beforeFilter();
        //echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><script language="javascript"> alert("Website đang trong quá trình nâng cấp, mời bạn quay trở lại sau !"); window.location.replace("'.DOMAIN.'"); </script>';
        $this->layout = "extent";

        $this->set("dataFilter", $this->dataFilter());
    }

    public function dataFilter()
    {
        return [
            "loainha" => [
                1 => "Nhà ống",
                2 => "Biệt thự  ",
                3 => "Văn phòng - khách sạn - Căn hộ dịch vụ",
            ],
            "phongcach" => [
                1 => "Hiện đại",
                2 => "Tân cổ điển",
                3 => "Á đông",
            ],
            "thuonghieu" => [
                1 => "Đồng tâm",
                2 => "Vigracera",
                3 => "Prime",
                4 => "Bạch mã",
                5 => "Nhập khẩu",
            ],
            "chungloai" => [
                1 => "Gạch lát trong nhà",
                2 => "Gạch lát ngoài nhà",
                3 => "Gạch mosaic",
            ],
            "khuvuc" => [
                1 => "Hà Nội",
                2 => "TP.HCM",
                3 => "Hải Phòng",
                4 => "Cần Thơ",
                5 => "Khác",
            ],
        ];
    }

    public function index($id = null)
    {
        if (empty($id)) $this->redirect(DOMAIN . 'err-page');
        $detailNews = $this->Catproduct->findByLink($id);
        if (empty($detailNews)) $this->redirect(DOMAIN . 'err-page');

        //set title, keyword, desciption
        $this->set_title_key_meta($detailNews['Catproduct']);

        //set limmit
        $limit = 20;
        //set bang nao
        $table = 'Product';

        $mnId = $this->multiMenuProduct($detailNews['Catproduct']['id'], null);
        $mnId[$detailNews['Catproduct']['id']] = $detailNews['Catproduct']['id'];

        $this->set('cat', $detailNews);

        if ($detailNews['Catproduct']['type'] == 'product') {
            //sap xep
            $order = 'order';
            if (count($mnId) > 1) $order = 'price';
            $this->paginate = array(
                'conditions' => array(
                    $table . '.status' => 1,
                    $table . '.cat_id' => $mnId
                ),
                'limit' => $limit,
                //'order' => $table.'.'.$order.' DESC'
                'order' => array($table . '.order DESC', $table . '.price DESC'),
            );

            $this->set('listnews', $this->paginate($table, array()));
            $this->render('list');
        }
        if ($detailNews['Catproduct']['type'] == 'new') {
            $limit = 10;
            $table = 'Post';

            $cnn = array();
            $cnn['Post.status'] = 1;
            $cnn['Post.cat_id'] = $mnId;

            $this->paginate = array(
                'conditions' => $cnn,
                'limit' => $limit,
                'order' => $table . '.order DESC'
            );

            $this->set('listnews', $this->paginate($table, array()));
            $this->render('/Post/list');
        }

        if (in_array($detailNews['Catproduct']['type'], ['newtwo', 'newfour'])) {
//            $check_parent = $this->Catproduct->find('first', array(
//                'conditions' => array(
//                    'Catproduct.status' => 1,
//                    'Catproduct.parent_id' => $detailNews['Catproduct']['id'],
//                )
//            ));
//            if (!empty($check_parent["Catproduct"]["id"])) {
            $parmenuProduct = $this->Catproduct->find('all', array(
                'conditions' => array(
                    'Catproduct.status' => 1,
                    'Catproduct.parent_id' => $detailNews['Catproduct']['id'],
                ),
                'fields' => array('id', 'name', 'link'),
                'order' => array('Catproduct.order' => 'ASC')
            ));
            $list_posts = array();

            foreach ($parmenuProduct as $values) {
//                    $mnId = $this->multiMenuProduct($values['Catproduct']['id'], null);
//                    $mnId[$values['Catproduct']['id']] = $values['Catproduct']['id'];

//                    $list = $this->Post->find('all', array(
//                        'conditions' => array(
//                            'Post.status' => 1,
//                            'Post.cat_id' => $mnId,
//                        ),
//                        'fields' => array('id', 'name', 'images', 'link'),
//                        'order' => array('Post.order' => 'DESC'),
//                        'limit' => 4
//                    ));
                //gop ten cha + noi dung
                $list_posts[] = array(
                    'id' => $values['Catproduct']['id'],
                    'name' => $values['Catproduct']['name'],
                    'link' => $values['Catproduct']['link'],
                    //'list' => $list
                );
            }
            $this->set('listnews', $list_posts);

            $limit = 25;
            $table = 'Post';

            $cnn = array();
            $cnn['Post.status'] = 1;
            $cnn['Post.cat_id'] = $mnId;

            if (!empty($_GET["q"]) && !empty($_GET["k"])) {
                $cnn["OR"] = array(
                    'Post.loainha LIKE' => '%|' . $_GET["q"] . '|%',
                    'Post.phongcach LIKE' => '%|' . $_GET["q"] . '|%'
                );
            } elseif (!empty($_GET["q"])) {
                $cnn['Post.loainha LIKE'] = '%|' . $_GET["q"] . '|%';
            } elseif (!empty($_GET["k"])) {
                $cnn['Post.phongcach LIKE'] = '%|' . $_GET["k"] . '|%';
            }

            if (!empty($_GET["t"]) && !empty($_GET["c"])) {
                $cnn["OR"] = array(
                    'Post.thuonghieu LIKE' => '%|' . $_GET["t"] . '|%',
                    'Post.chungloai LIKE' => '%|' . $_GET["c"] . '|%'
                );
            } elseif (!empty($_GET["t"])) {
                $cnn['Post.thuonghieu LIKE'] = '%|' . $_GET["t"] . '|%';
            } elseif (!empty($_GET["c"])) {
                $cnn['Post.chungloai LIKE'] = '%|' . $_GET["c"] . '|%';
            }

            $this->paginate = array(
                'conditions' => $cnn,
                'limit' => $limit,
                'order' => $table . '.order DESC'
            );

            $this->set('listposts', $this->paginate($table, array()));

            if (in_array($detailNews['Catproduct']['type'], ['newtwo'])) {
                $this->render('/Post/Type1/list');
            } else {
                $this->render('/Post/Type2/list');
            }
//            } else {
//                $limit = 8;
//                $table = 'Post';
//                $this->paginate = array(
//                    'conditions' => array(
//                        $table . '.status' => 1,
//                        $table . '.cat_id' => $mnId
//                    ),
//                    'limit' => $limit,
//                    'order' => $table . '.order DESC'
//                );
//
//                $this->set('listnews', $this->paginate($table, array()));
//                $this->render('/Post/list');
//            }
        }

        if (in_array($detailNews['Catproduct']['type'], ['newthree'])) {
            $check_parent = $this->Catproduct->find('first', array(
                'conditions' => array(
                    'Catproduct.status' => 1,
                    'Catproduct.parent_id' => $detailNews['Catproduct']['id'],
                )
            ));
            if (!empty($check_parent["Catproduct"]["id"])) {
                $listCat = $this->Catproduct->find('all', array(
                    'conditions' => array(
                        'Catproduct.status' => 1,
                        'Catproduct.parent_id' => $detailNews['Catproduct']['id'],
                    ),
                    'fields' => array('id', 'name', 'link', 'images'),
                    'order' => array('Catproduct.order' => 'ASC')
                ));
                $this->set('listCat', $listCat);
                $this->render('/Post/Type3/list');
            } else {
                $limit = 10;
                $table = 'Post';
                $cnn = array();
                $cnn['Post.status'] = 1;
                $cnn['Post.cat_id'] = $mnId;

                if (!empty($_GET["kv"])) {
                    $cnn['Post.khuvuc LIKE'] = '%|' . $_GET["kv"] . '|%';
                }

                $this->paginate = array(
                    'conditions' => $cnn,
                    'limit' => $limit,
                    'order' => $table . '.order DESC'
                );

                $this->set('listposts', $this->paginate($table, array()));

                $this->render('/Post/Type3/list_post');
            }
        }

        if ($detailNews['Catproduct']['type'] == 'video') {
            $limit = 20;
            $table = 'Video';
            $this->paginate = array(
                'conditions' => array(
                    $table . '.status' => 1
                ),
                'limit' => $limit,
                'order' => $table . '.order DESC'
            );

            $this->set('listnews', $this->paginate($table, array()));
            $this->render('/Video/list');
        }
    }

    public function timkiem()
    {
        if (isset($_POST['tukhoa'])) {
            $this->Session->write('tukhoa', $_POST['tukhoa']);
            $tukhoa = $_POST['tukhoa'];
        } else {
            $tukhoa = $this->Session->read('tukhoa');
        }

        $cnn = array();

        if (!empty($tukhoa)) {
            $cnn['Post.name LIKE'] = '%' . $tukhoa . '%';
        }

        $cnn['Post.status'] = 1;
        $cnn['Post.type'] = array("posttwo", "postfour");

        $limit = 10;
        $table = 'Post';
        $this->paginate = array(
            'conditions' => $cnn,
            'limit' => $limit,
            'order' => $table . '.order DESC'
        );

        $this->set('listnews', $this->paginate($table, array()));

        $this->set('title_for_layout', 'Tìm kiếm');
        $this->layout = 'extent';
        $this->render('/Post/list_timkiem');
    }

    public function hang($id = null)
    {
        if (empty($id)) $this->redirect(DOMAIN . 'err-page');
        $this->loadModel('Hang');
        $detailNews = $this->Hang->findByLink($id);
        if (empty($detailNews)) $this->redirect(DOMAIN . 'err-page');

        //set title, keyword, desciption
        $this->set_title_key_meta($detailNews['Hang']);

        //set limmit
        $limit = 40;
        //set bang nao
        $table = 'Product';

        $this->paginate = array(
            'conditions' => array(
                $table . '.status' => 1,
                $table . '.hang_id' => $detailNews['Hang']['id']
            ),
            'limit' => $limit,
            'order' => $table . '.order DESC'
        );

        $this->set('listnews', $this->paginate($table, array()));
        $this->set('cat', $detailNews);
        $this->render('list');
    }

    public function detail($id = null)
    {
        if (empty($id)) $this->redirect(DOMAIN . 'err-page');
        $detailNews = $this->Product->findByLink($id);
        if (empty($detailNews)) $this->redirect(DOMAIN . 'err-page');

        //set luot xem
        if (!$this->Session->check('id_view' . $detailNews['Product']['id'])) {
            $this->Product->id = $detailNews['Product']['id'];
            $this->Product->saveField('view', ($detailNews['Product']['view'] + 1));
        }
        $this->Session->write('id_view' . $detailNews['Product']['id'], $detailNews['Product']['id']);

        $this->set('detailNews', $detailNews);
        if (!empty($detailNews['Product']['cat_id'])) {
            $cat_parent = $this->Catproduct->findById($detailNews['Catproduct']['parent_id']);
            $this->set('cat_parent', $cat_parent);
        }

        //set limmit
        $limit = 8;

        $this->paginate = array(
            'conditions' => array(
                'Product.status' => 1,
                'Product.id <>' => $detailNews['Product']['id'],
                'Product.cat_id' => $detailNews['Product']['cat_id']
            ),
            'order' => 'Product.order DESC', 'limit' => $limit
        );
        $this->set('tinlienquan', $this->paginate('Product', array()));

        //set title, keyword, desciption
        $this->set_title_key_meta($detailNews['Product']);
    }

    public function detailnewOther($id = null)
    {
        $this->layout = "detail";
        if (empty($id)) $this->redirect(DOMAIN . 'err-page');
        $detailNews = $this->Post->findByLink($id);
        if (empty($detailNews)) $this->redirect(DOMAIN . 'err-page');

        //set luot xem
        if (!$this->Session->check('id_view' . $detailNews['Post']['id'])) {
            $this->Post->id = $detailNews['Post']['id'];
            $this->Post->saveField('view', ($detailNews['Post']['view'] + 1));
        }
        $this->Session->write('id_view' . $detailNews['Post']['id'], $detailNews['Post']['id']);

        $this->set('detailNews', $detailNews);

        //set limmit
        $limit = 10;

        $tinlienquan = $this->Post->find('all', array(
            'conditions' => array(
                'Post.status' => 1,
                'Post.id <>' => $detailNews['Post']['id'],
                'Post.cat_id' => $detailNews['Post']['cat_id']
            ),
            'order' => 'Post.order DESC', 'limit' => $limit
        ));
        $this->set('tinlq', $tinlienquan);

        //set title, keyword, desciption
        $this->set_title_key_meta($detailNews['Post']);

        $this->render('/Post/detail_other');
    }

    public function detailnew($id = null)
    {
        $this->layout = "detail";
        if (empty($id)) $this->redirect(DOMAIN . 'err-page');
        $detailNews = $this->Post->findByLink($id);
        if (empty($detailNews)) $this->redirect(DOMAIN . 'err-page');

        //set luot xem
        if (!$this->Session->check('id_view' . $detailNews['Post']['id'])) {
            $this->Post->id = $detailNews['Post']['id'];
            $this->Post->saveField('view', ($detailNews['Post']['view'] + 1));
        }
        $this->Session->write('id_view' . $detailNews['Post']['id'], $detailNews['Post']['id']);

        $this->set('detailNews', $detailNews);

        //set limmit
        $limit = 10;

        $tinlienquan = $this->Post->find('all', array(
            'conditions' => array(
                'Post.status' => 1,
                'Post.id <>' => $detailNews['Post']['id'],
                'Post.cat_id' => $detailNews['Post']['cat_id']
            ),
            'order' => 'Post.order DESC', 'limit' => $limit
        ));
        $this->set('tinlq', $tinlienquan);

        $tinmoiup = $this->Post->find('all', array(
            'conditions' => array(
                'Post.status' => 1,
                'Post.type' => 'post',
            ),
            'order' => 'Post.order DESC',
            'limit' => $limit
        ));
        $this->set('tinmoiup', $tinmoiup);

        //set title, keyword, desciption
        $this->set_title_key_meta($detailNews['Post']);

        $this->render('/Post/detail');
    }

    public function lienhe($id = null)
    {
        $abc = $this->Catproduct->findById($id);
        $this->set('cat', $abc);

        //set title, keyword, desciption
        $this->set_title_key_meta($abc['Catproduct']);
    }

    public function addshopingcart($id = null)
    {
        $this->layout = false;
        $product = $this->Product->read(null, $id);
        if (!isset($_SESSION['shopingcart']))
            $_SESSION['shopingcart'] = array();
        if (isset($_SESSION['shopingcart'])) {
            $shopingcart = $_SESSION['shopingcart'];
            if (isset($shopingcart[$id])) {
                if (isset($_POST['soluong'])) $shopingcart[$id]['sl'] = $_POST['soluong']; else $shopingcart[$id]['sl'] = 1;
                $shopingcart[$id]['total'] = $shopingcart[$id]['price'] * $shopingcart[$id]['sl'];
                $_SESSION['shopingcart'] = $shopingcart;
                echo '<script language="javascript"> window.location.replace("' . DOMAIN . 'hien-gio-hang-cua-mat-hang.html"); </script>';
                exit; // co the thay DOMAIN bang url ban muon chay toi
            } else {
                //viet nam
                $shopingcart[$id]['name'] = $product['Product']['name'];
                $shopingcart[$id]['id'] = $product['Product']['id'];
                $shopingcart[$id]['images'] = DOMAIN . 'img/w80/' . $product['Product']['images'];
                if (isset($_POST['soluong'])) $shopingcart[$id]['sl'] = $_POST['soluong']; else $shopingcart[$id]['sl'] = 1;
                $shopingcart[$id]['price'] = $product['Product']['price'];
                $shopingcart[$id]['content'] = $product['Product']['content'];
                $shopingcart[$id]['total'] = $shopingcart[$id]['price'] * $shopingcart[$id]['sl'];
                $_SESSION['shopingcart'] = $shopingcart;
                $this->set(compact('shopingcart'));
                echo '<script language="javascript"> window.location.replace("' . DOMAIN . 'hien-gio-hang-cua-mat-hang.html"); </script>';
                exit;
            }
        }
    }

    public function viewshopingcart()
    {
        $this->set('title_for_layout', 'Giỏ hàng của bạn');
        $this->set('giohang', 'Giỏ hàng của bạn');
        if (isset($_SESSION['shopingcart'])) {
            $shopingcart = $_SESSION['shopingcart'];
            $this->set(compact('shopingcart'));
        } else {
            echo '<script language="javascript"> alert("Chua co san pham nao trong gio hang"); location.href="' . DOMAIN . '"; </script>';
        }
    }

    public function deleteshopingcart($id = null)
    {
        if (isset($_SESSION['shopingcart'])) {
            $shopingcart = $_SESSION['shopingcart'];
            if (isset($shopingcart[$id]))
                unset($shopingcart[$id]);
            $_SESSION['shopingcart'] = $shopingcart;
            $this->redirect($this->referer());
        }

    }

    public function updateshopingcart($id = null)
    {

        if (isset($_SESSION['shopingcart'])) {
            $shopingcart = $_SESSION['shopingcart'];
            if (isset($shopingcart[$id])) {
                $shopingcart[$id]['sl'] = $_POST['soluong'];
                $shopingcart[$id]['total'] = $shopingcart[$id]['sl'] * $shopingcart[$id]['price'];
            }
            $_SESSION['shopingcart'] = $shopingcart;

            $this->redirect($this->referer());
        }
    }

    public function buy()
    {
        $this->set('title_for_layout', 'Giỏ hàng của bạn');
        $this->set('giohang', 'Giỏ hàng của bạn');
        if (isset($_SESSION['shopingcart'])) {
            $shopingcart = $_SESSION['shopingcart'];
            $this->set(compact('shopingcart'));
        } else {
            echo '<script language="javascript"> alert("Product emty !"); window.location.replace("' . DOMAIN . '"); </script>';
        }
    }

    public function multiParentNull($parentid = null, $trees = NULL)
    {
        $this->loadModel('Catproduct');
        $parmenu = $this->Catproduct->find('first', array(
            'conditions' => array(
                'Catproduct.id' => $parentid,
                'Catproduct.status' => 1
            ),
            'order' => 'Catproduct.order ASC'
        ));
        if (count($parmenu) > 0) {
            if (empty($parmenu['Catproduct']['parent_id'])) {
                $trees = $parmenu['Catproduct']['id'];
            } else {
                $trees = $this->multiParentNull($parmenu['Catproduct']['parent_id']);
            }
        } else {
            $trees = $parentid;
        }
        return $trees;
    }

    public function multiMenuProduct($parentid = null, $trees = NULL)
    {
        $parmenu = $this->Catproduct->find('all', array(
            'conditions' => array(
                'Catproduct.parent_id' => $parentid,
                'Catproduct.status' => 1
            ),
            'order' => 'Catproduct.order ASC'
        ));
        if (count($parmenu) > 0) {
            foreach ($parmenu as $field) {
                $trees[$field['Catproduct']['id']] = $field['Catproduct']['id'];
                $trees = $this->multiMenuProduct($field['Catproduct']['id'], $trees);
            }
        }
        return $trees;
    }
}

