<?php

/**
 * Description of HomeComtroller
 * @author : Yeu tinh
 * @since Math 11, 2013
 */

class VideoController extends AppController {

    public $name = 'Video';
    public $uses = array('Video');


    public function beforeFilter() {
        parent::beforeFilter();
        
    }

    public function detail($id = null) {
    	if(empty($id)) $this->redirect(DOMAIN.'err-page');
        $detailNews = $this->Video->findById($id);
        if(empty($detailNews)) $this->redirect(DOMAIN.'err-page');

        $this->set('detailNews', $detailNews);

        //set limmit
        $limit  = 8;

		$this->paginate = array(
            'conditions'=>array(
                'Video.status'=>1, 
                'Video.id <>' => $detailNews['Video']['id'],
            ),
                'order' => 'Video.order DESC','limit' => $limit
        );
        $this->set('tinlienquan', $this->paginate('Video', array()));	
    }
}