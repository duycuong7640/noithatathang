<?php

/**

 * Application level Controller

 *

 * This file is application-wide controller file. You can put all

 * application-wide controller-related methods here.

 *

 * PHP 5

 *

 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)

 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)

 *

 * Licensed under The MIT License

 * Redistributions of files must retain the above copyright notice.

 *

 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)

 * @link          http://cakephp.org CakePHP(tm) Project

 * @package       app.Controller

 * @since         CakePHP(tm) v 0.2.9

 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)

 */



App::uses('Controller', 'Controller');



/**

 * Application Controller

 *

 * Add your application-wide methods in the class below, your controllers

 * will inherit them.

 *

 * @package       app.Controller

 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller

 */

class AppController extends Controller {
    
	public $ext = '.php';

    public function beforeFilter() {

        parent::beforeFilter();

        $mbm = 1;
        $mobile_title = '';
        $mobile_get = '';
        $user_agent = strtolower($_SERVER['HTTP_USER_AGENT']);
        if (preg_match("/phone|iphone|itouch|ipod|ipad|symbian|android|htc_|htc-|palmos|blackberry|opera mini|iemobile|windows ce|nokia|fennec|hiptop|kindle|mot |mot-|webos\/|samsung|sonyericsson|^sie-|nintendo/", $user_agent)) {
            $mbm = 0;
            $mobile_title = 'mobile_';
            $mobile_get = '&mb=1';
        }

        if (!empty($_GET['mb'])) {
            $mbm = 0;
            $mobile_title = 'mobile_';
            $mobile_get = '&mb=1';
        }

        $this->set('mbm', $mbm);

        //cache html
//        $folderRoot = ROOT . DS . 'app' . DS . 'webroot' . DS . 'file_html_cache' . DS;
//        $here = substr($this->request->here, 1);
//        $urlHere = DOMAIN . $here;
//        $here = !empty($here) ? $here : $mobile_title.'home';
//        $urlCache = $folderRoot . $mobile_title . str_replace('/', '-', str_replace('.', '', $here)) . '.html';
//        if (isset($_GET['cache_html'])) {
//            $get = $this->getHtml($urlHere . '?load_html=1' . $mobile_get);
//            file_put_contents($urlCache, $get);
//            die('Success');
//        } else {
//            if (!isset($_GET['load_html'])) {
//                if (file_exists($urlCache)) {
//                    $html = file_get_contents($urlCache);
//                    echo $html;
//                    die();
//                } else {
//                    $this->getHtml($urlHere . '?cache_html=1' . $mobile_get);
//                    header('Location: ' . $urlHere);
//                    die;
//                }
//            }
//        }
        
        //set menu
        $app_menu = $this->multiMenu(null, null);
        $this->set('app_menu', $app_menu);
        
        $this->loadModel('Slideshow');
        $this->loadModel('Catproduct');
        $this->loadModel('Extention');
        $this->loadModel('Support');
        $this->loadModel('Post'); 
        
        //load support
            /*$hotrotructuyen = $this->Support->find('all', array(
                'conditions' => array(
                    'Support.status' => 1,
                ),
                'order' => 'Support.id ASC',
            ));
            $this->set('hotrotructuyen', $hotrotructuyen);*/

        /*
            Tin tuc
        */
            $list_tintuchome = $this->Post->find('all', array(
                'conditions' => array(
                    'Post.status' => 1,
                    'Post.type' => 'post',
                    'Post.choose2' => 1,
                ),
                'order' => array('Post.order' => 'DESC'),
                'limit' => 5
            ));
            $this->set('list_tintuchome', $list_tintuchome);
        
        //load danh muc 
            $danhmuc = $this->Catproduct->find('all', array(
                'conditions' => array(
                    'Catproduct.status' => 1,
                    'Catproduct.type' => array('', 'new', 'product'),
                    'Catproduct.parent_id' => null,
                ),
                'order' => 'Catproduct.order ASC',
            ));
            $this->set('danhmuc', $danhmuc);
            
        //menu left
            /*$parmenuProduct = $this->Catproduct->find('all', array(
                'conditions' => array(
                    'Catproduct.status' => 1,
                    'Catproduct.dang1' => 1,
                    'Catproduct.parent_id' => null,
                ),
                'fields' => array('id', 'name', 'link'),
                'order' => array('Catproduct.order' => 'ASC'),
                'limit' => '2'
            )); $list_menu_footer = array();
            foreach($parmenuProduct as $values){
                $list = $this->Catproduct->find('all', array(
                    'conditions' => array(
                        'Catproduct.status' => 1,
                        'Catproduct.parent_id' => $values['Catproduct']['id'],
                    ),
                    'fields' => array('id', 'name', 'link'),
                    'order' => array('Catproduct.order' => 'ASC')
                ));
                //gop ten cha + noi dung
                $list_menu_footer[] = array(
                    'id' => $values['Catproduct']['id'],
                    'name' => $values['Catproduct']['name'],
                    'link' => $values['Catproduct']['link'],
                    'list' => $list
                );
           }
           $this->set('list_menu_footer', $list_menu_footer);*/
        
        //load banner, logo
            $banner = $this->Extention->find('first', array(
                'conditions' => array(
                    'Extention.status' => 1,
                    'Extention.type' => 'banner'
                ),
            )); 
            $this->set('banner', $banner);
            
            /*$chaytrai = $this->Extention->find('first', array(
                'conditions' => array(
                    'Extention.status' => 1,
                    'Extention.type' => 'advone'
                ),
            )); 
            $this->set('chaytrai', $chaytrai);
            
            $chayphai = $this->Extention->find('first', array(
                'conditions' => array(
                    'Extention.status' => 1,
                    'Extention.type' => 'advtwo'
                ),
            )); 
            $this->set('chayphai', $chayphai);*/
            
        //load quang cao trai
            /*$quangcaotrai = $this->Extention->find('all', array(
                'conditions' => array(
                    'Extention.status' => 1,
                    'Extention.type' => 'advleft'
                ),
            )); 
            $this->set('quangcaotrai', $quangcaotrai);*/

        //load quang cao phai
            $quangcaophai = $this->Extention->find('all', array(
                'conditions' => array(
                    'Extention.status' => 1,
                    'Extention.type' => 'advthree'
                ),
            )); 
            $this->set('quangcaophai', $quangcaophai);
        
        //slide
            $slideshow = $this->Slideshow->find('all', array(
                'conditions' => array(
                    'Slideshow.status' => 1,
                ),
                    'order' => 'Slideshow.order DESC'
            )); 
            $this->set('slideshow', $slideshow);
        
        //set gia tri setting website
        $this->loadModel('Setting');
        $setting = $this->Setting->find('first');
        if($setting != null){
            $this->set('title_for_layout', $setting['Setting']['title']);
            $this->set('keywords_for_layout', $setting['Setting']['meta_key']);
            $this->set('description_for_layout', $setting['Setting']['meta_des']);
            $this->set('setting', $setting['Setting']);
        }
    }

    function getHtml($url)
    {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    public function set_title_key_meta($arr = array()){
        if(!empty($arr['title_seo'])) $this->set('title_for_layout', $arr['title_seo']); else $this->set('title_for_layout', $arr['name']);
        if($arr['meta_key']) $this->set('keywords_for_layout', $arr['meta_key']);
        if($arr['meta_des']) $this->set('description_for_layout', $arr['meta_des']);
    }    

    public function multiMenu($parentid = null, $trees = NULL) {
        $this->loadModel('Catproduct');
        $parmenu = $this->Catproduct->find('all', array(
            'fields' => array('id', 'name', 'link'),
            'conditions' => array(
                'Catproduct.parent_id' => $parentid,
                'Catproduct.status' => 1,
                'Catproduct.dang1' => 0,
                'Catproduct.type' => array('', 'new', 'product', 'newtwo', 'newthree', 'newfour'),
            ),
            'order' => 'Catproduct.order ASC'
            ));
        if (count($parmenu) > 0) {
            $trees .='<ul>';
            foreach ($parmenu as $field) {
                if($parentid != null){
                    $trees .= '<li><a href="' . DOMAIN . $field['Catproduct']['link'] .  '" title="'. $field['Catproduct']['name'].'">' . $field['Catproduct']['name'.$this->Session->read('lang')] . '</a>';
                    $trees = $this->multiMenu($field['Catproduct']['id'], $trees);
                    $trees .='</li>';
                }else{
                    //active
                    $link_here = $this->request->url;
                    if($link_here == ''){
                        //trang chu
                        $id = 1;
                    }else{
                        if(strpos($link_here, '.htm')){
                            $link_here = substr($link_here, 0, strpos($link_here, '.htm'));

                        }elseif(strpos($link_here, '.html')){
                            $link_here = substr($link_here, 0, strpos($link_here, '.html'));
                            
                        }else{

                        }
                    }

                    $active = null;
                    if($field['Catproduct']['id'] == $this->Session->read('active_mn')) $active = 'class="active_mn"';

                    if($field['Catproduct']['id'] == 1){
                        $trees .= '<li '.$link_here.'><a class="parent1" href="' . DOMAIN . '" title="'. $field['Catproduct']['name'].'">' . $field['Catproduct']['name'.$this->Session->read('lang')] . '</a>';
                    }else{
                        $mn_child = $this->Catproduct->findByParent_id($field['Catproduct']['id']);
                        $i = null;
                        if(!empty($mn_child)) $i = '<i class="fa fa-caret-down"></i>';

                        $trees .= '<li '.$link_here.'><a class="parent1" href="' . DOMAIN . $field['Catproduct']['link'] . '" title="'. $field['Catproduct']['name'].'">' . $field['Catproduct']['name'.$this->Session->read('lang')] . $i .'</a>';    
                    }
                    $trees = $this->multiMenu($field['Catproduct']['id'], $trees);
                    $trees .='</li>';
                }
            }
            $trees .='</ul>';
        }
        return $trees;
    }
    
    public function multiMenuLeft($parentid = null, $trees = NULL) {
        $this->loadModel('Catproduct');
        $parmenu = $this->Catproduct->find('all', array(
            'fields' => array('id', 'name', 'link', 'images'),
            'conditions' => array(
                'Catproduct.parent_id' => $parentid,
                'Catproduct.status' => 1,
                'Catproduct.type' => 'product',
                'Catproduct.dang1' => 0,
            ),
            'order' => 'Catproduct.order ASC'
            ));
        if (count($parmenu) > 0) {
            $trees .='<ul>';
            foreach ($parmenu as $field) {
                if($parentid != null){
                    $trees .= '<li><a href="' . DOMAIN . $field['Catproduct']['link'] .  '" title="'. $field['Catproduct']['name'].'"><i class="fa fa-angle-right"></i><h2>' . $field['Catproduct']['name'.$this->Session->read('lang')] . '</h2></a>';
                    $trees = $this->multiMenuLeft($field['Catproduct']['id'], $trees);
                    $trees .='</li>';
                }else{
                    $trees .= '<li><a class="parent1" href="' . DOMAIN . $field['Catproduct']['link'] . '" title="'. $field['Catproduct']['name'].'"><h2>' . $field['Catproduct']['name'.$this->Session->read('lang')] . '</h2></a>';
                    $trees = $this->multiMenuLeft($field['Catproduct']['id'], $trees);
                    $trees .='</li>';
                }
            }
            $trees .='</ul>';
        }
        return $trees;
    }
    
    public function multiMenuFooter($parentid = null, $trees = NULL) {
        $this->loadModel('Catproduct');
        $parmenu = $this->Catproduct->find('all', array(
            'fields' => array('id', 'name', 'link', 'images'),
            'conditions' => array(
                'Catproduct.parent_id' => $parentid,
                'Catproduct.status' => 1,
                'Catproduct.dang1' => 0,
            ),
            'order' => 'Catproduct.order ASC'
            ));
        if (count($parmenu) > 0) {
            $trees .='<ul>';
            foreach ($parmenu as $field) {
                if($parentid != null){
                    $trees .= '<li><a href="' . DOMAIN . $field['Catproduct']['link'] .  '" title="'. $field['Catproduct']['name'].'"><i class="fa fa-angle-right"></i><h4>' . $field['Catproduct']['name'.$this->Session->read('lang')] . '</h4></a>';
                    $trees = $this->multiMenuFooter($field['Catproduct']['id'], $trees);
                    $trees .='</li>';
                }else{
                    $trees .= '<li><a class="parent1" href="' . DOMAIN . $field['Catproduct']['link'] . '" title="'. $field['Catproduct']['name'].'"><h4>' . $field['Catproduct']['name'.$this->Session->read('lang')] . '</h4></a>';
                    $trees = $this->multiMenuFooter($field['Catproduct']['id'], $trees);
                    $trees .='</li>';
                }
            }
            $trees .='</ul>';
        }
        return $trees;
    }
    
    public function multiParentNull($parentid = null, $trees = NULL) {
        $this->loadModel('Catproduct');
        $parmenu = $this->Catproduct->find('first', array(
            'conditions' => array(
                'Catproduct.id' => $parentid,
                'Catproduct.status' => 1
            ),
            'order' => 'Catproduct.order ASC'
            ));
        if (count($parmenu) > 0) {
            if(empty($parmenu['Catproduct']['parent_id'])){
                $trees = $parmenu['Catproduct']['id'];
            }else{
                $trees = $this->multiParentNull($parmenu['Catproduct']['parent_id']);
            }
        }
        return $trees;
    }

    public function multiMenuProduct($parentid = null, $trees = NULL) {
        $this->loadModel('Catproduct');
        $parmenu = $this->Catproduct->find('all', array(
            'conditions' => array(
                'Catproduct.parent_id' => $parentid,
                'Catproduct.status' => 1
            ),
            'order' => 'Catproduct.order ASC'
            ));
        if (count($parmenu) > 0) {
            foreach ($parmenu as $field) {
                $trees[$field['Catproduct']['id']] = $field['Catproduct']['id'];
                $trees = $this->multiMenuProduct($field['Catproduct']['id'], $trees);
            }
        }
        return $trees;
    }

}

