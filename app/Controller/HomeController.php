<?php
/**
 * Description of HomeComtroller
 * @author : Yeu tinh
 * @since Math 11, 2013
 */

class HomeController extends AppController
{

    public $name = 'Home';
    public $uses = array('Catproduct', 'Product', 'Post', 'Extention', 'Video');


    public function beforeFilter()
    {
        parent::beforeFilter();

        /*for($i= 0; $i <10; $i ++){
            $data['Kiemchuyen']['name'] = 'cường'.$i;
            $this->Post->save($data['Kiemchuyen']);
        }
        pr($this->Post->query('SELECT * FROM itaboutique_kiemchuyen')); die;*/
    }

    public function index()
    {
        $this->set('trangchu', 'trangchu');

        //thiet ke
        $thietke = $this->Catproduct->find('all', array(
            'conditions' => array(
                'Catproduct.status' => 1,
                'Catproduct.home' => 1,
            ),
            'fields' => array('id', 'name', 'images', 'link', 'content'),
            'order' => array('Catproduct.order' => 'ASC')
        ));
        $arr = array();

        foreach ($thietke as $values) {
            $list = $this->Catproduct->find('all', array(
                'conditions' => array(
                    'Catproduct.status' => 1,
                    'Catproduct.parent_id' => $values['Catproduct']['id'],
                ),
                'fields' => array('id', 'name', 'images', 'link', 'content'),
                'order' => array('Catproduct.order' => 'DESC'),
            ));
            //gop ten cha + noi dung
            $arr[] = array(
                'id' => $values['Catproduct']['id'],
                'name' => $values['Catproduct']['name'],
                'link' => $values['Catproduct']['link'],
                'images' => $values['Catproduct']['images'],
                'content' => $values['Catproduct']['content'],
                'list' => $list
            );
        }

        foreach ($arr as $row) {
            if (!empty($row["list"])) {
                $list_thietke["one"][] = $row;
            } else {
                $list_thietke["two"][] = $row;
            }
        }
        $this->set('list_thietke', $list_thietke);

//        /*
//            Gioi thieu
//        */
//        $list_videohome = $this->Video->find('all', array(
//            'conditions' => array(
//                'Video.status' => 1
//            ),
//            'order' => array('Video.id' => 'DESC'),
//            'limit' => 4
//        ));
//        $this->set('list_videohome', $list_videohome);
//
//        /*
//            Cam nhan
//        */
//        $list_camnhanhome = $this->Post->find('all', array(
//            'conditions' => array(
//                'Post.status' => 1,
//                'Post.type' => 'post',
//                'Post.choose3' => 1,
//            ),
//            'order' => array('Post.order' => 'DESC'),
//            'limit' => 40
//        ));
//        $this->set('list_camnhanhome', $list_camnhanhome);
//
//        /*
//            San pham
//        */
//        $parmenuProduct = $this->Catproduct->find('all', array(
//            'conditions' => array(
//                'Catproduct.status' => 1,
//                'Catproduct.home' => 1,
//                'Catproduct.type' => 'product',
//            ),
//            'fields' => array('id', 'name', 'link'),
//            'order' => array('Catproduct.order' => 'ASC')
//        ));
//        $list_product = array();
//
//        foreach ($parmenuProduct as $values) {
//            $mnId = $this->multiMenuProduct($values['Catproduct']['id'], null);
//            $mnId[$values['Catproduct']['id']] = $values['Catproduct']['id'];
//
//            $list = $this->Product->find('all', array(
//                'conditions' => array(
//                    'Product.status' => 1,
//                    'Product.choose1' => 1,
//                    'Product.cat_id' => $mnId,
//                ),
//                'fields' => array('id', 'name', 'images', 'link'),
//                'order' => array('Product.order' => 'DESC'),
//                'limit' => 4
//            ));
//            //gop ten cha + noi dung
//            $list_product[] = array(
//                'id' => $values['Catproduct']['id'],
//                'name' => $values['Catproduct']['name'],
//                'link' => $values['Catproduct']['link'],
//                'list' => $list
//            );
//        }
//        $this->set('list_product', $list_product);
//
//        /*
//            Cam nhan
//        */
//        $parmenuNew = $this->Catproduct->find('all', array(
//            'conditions' => array(
//                'Catproduct.status' => 1,
//                'Catproduct.home' => 1,
//                'Catproduct.type' => 'new',
//            ),
//            'fields' => array('id', 'name', 'link'),
//            'order' => array('Catproduct.order' => 'ASC')
//        ));
//        $list_newhome = array();
//
//        foreach ($parmenuNew as $values) {
//            $mnId = $this->multiMenuProduct($values['Catproduct']['id'], null);
//            $mnId[$values['Catproduct']['id']] = $values['Catproduct']['id'];
//
//            $list = $this->Post->find('all', array(
//                'conditions' => array(
//                    'Post.status' => 1,
//                    'Post.choose1' => 1,
//                    'Post.cat_id' => $mnId,
//                ),
//                'fields' => array('id', 'name', 'images', 'link'),
//                'order' => array('Post.order' => 'DESC'),
//                'limit' => 4
//            ));
//            //gop ten cha + noi dung
//            $list_newhome[] = array(
//                'id' => $values['Catproduct']['id'],
//                'name' => $values['Catproduct']['name'],
//                'link' => $values['Catproduct']['link'],
//                'list' => $list
//            );
//        }
//        $this->set('list_newhome', $list_newhome);

    }

    public function multiMenuProduct($parentid = null, $trees = NULL)
    {
        $parmenu = $this->Catproduct->find('all', array(
            'fields' => array('id', 'name'),
            'conditions' => array(
                'Catproduct.parent_id' => $parentid,
                'Catproduct.status' => 1
            ),
            'order' => 'Catproduct.order ASC'
        ));
        if (count($parmenu) > 0) {
            foreach ($parmenu as $field) {
                $trees[$field['Catproduct']['id']] = $field['Catproduct']['id'];
                $trees = $this->multiMenuProduct($field['Catproduct']['id'], $trees);
            }
        }
        return $trees;
    }
}