<div class="bg-cat">
    <div class="bg-title-cat">
        <h1><?php echo $detailNews['Video']['name'];?></h1>
    </div>
</div>
<div class="list-video">
    <iframe width="728" height="410" src="https://www.youtube.com/embed/<?php echo get_youtubeid($detailNews['Video']['video']);?>?rel=0&amp;showinfo=0&amp;vq=large&amp;iv_load_policy=3&amp;modestbranding=1&amp;autohide=1&amp;nologo=1&amp;autoplay=1" frameborder="0" allowfullscreen></iframe>
    <div class="clear-main"></div>
</div>

<div class="bg-cat">
    <div class="bg-title-cat">
        <label>Video liên quan</label>
    </div>
</div>
<div class="list-sphome">
    <ul>
        <?php foreach($tinlienquan as $value){?>
            <li>
                <div class="b-listsp">
                    <div class="b-img-sp">
                        <a href="<?php echo DOMAIN.'detail/'.$value['Video']['id'];?>" title="<?php echo $value['Video']['name'];?>">
                            <img class="image" src="<?php echo video_image($value['Video']['video']); ?>" title="<?php echo $value['Video']['name'];?>" alt="<?php echo $value['Video']['name'];?>"/>
                        </a>
                    </div>
                    <a href="<?php echo DOMAIN.'detail/'.$value['Video']['id'];?>" title="<?php echo $value['Video']['name'];?>">
                        <h3><?php echo $value['Video']['name'];?></h3>
                    </a>
                </div>
            </li>
        <?php }?>
    </ul>
    <div class="clear-main"></div>
    <?php if($this->Paginator->numbers()){?>
        <div class="pagination">
            <?php
                echo $this->Paginator->first('« Đầu');     
                echo $this->Paginator->prev('« Trước', null, null, array('class' => 'disabled')); 
                echo $this->Paginator->numbers(
                    array(
                        'before' => null,
                        'after' => null,
                        
                        'tag' => 'span',
                        'class' => 'number',
                        'modulus' => '6',
                        'separator' => null,
                        'first' => 2,
                        'last' => 2,
                        'ellipsis' => '...',
                        'currentClass' => 'current',
                        'currentTag' => null
                    )
                );
                echo $this->Paginator->next('Tiếp »'); 
                echo $this->Paginator->last('Cuối »'); 
                echo $this->Paginator->counter('Trang {:page}/{:pages}. Đang xem {:current}/{:count}.');
            ?>
        </div>
    <?php }?>
    <div class="clear-main"></div>
</div>