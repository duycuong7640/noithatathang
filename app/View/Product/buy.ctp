<div class="bg-cat">
    <div class="bg-title-cat bg-title-catff">
        <h1>Giỏ hàng</h1>
    </div>
</div>
<div class="padd-spcat">
    <div class="b-product-list padd-b-product-list">
        <div class="ct-tt">
            <table width="100%" border="1" cellspacing="0" cellpadding="0" class="table table-striped table-bordered">
                <tr>
                    <th width="90"><div class="title_giohang" align="center"><?php echo lang('hinhanh');?></div></th>
                    <th width="120"><div class="title_giohang" align="center"><?php echo lang('tensanpham');?></div></th>
                    <th width="80"><div class="title_giohang" align="center"><?php echo lang('gia');?></div></th>
                    <th width="80"><div class="title_giohang" align="center"><?php echo lang('Size');?></div></th>
                    <th width="80"><div class="title_giohang" align="center"><?php echo lang('Color');?></div></th>
                    <th width="57"><div class="title_giohang" align="center"><?php echo lang('soluong');?></div></th>
                    <th width="96"><div class="title_giohang" align="center"><?php echo lang('tongtien');?></div></th>
                    <th width="55"><div class="title_giohang" align="center"><?php echo lang('capnhat');?></div></th>
                    <th width="50"><div class="title_giohang" align="center"><?php echo lang('xuly');?></div></th>
                </tr>
                <?php $total=0; $i=0; foreach($shopingcart as $key=>$product) {?>
                <tr>
                    <td><div class="nd_giohang" align="center"><img width="60" src="<?php echo $product['images']; ?>" /></div></td>
                    <td><div class="nd_giohang" align="left"><?php echo $product['name'];?></div></td>
                    <td><div class="nd_giohang" align="center"><?php echo number_format($product['price']);?> VNĐ</div></td>
                    <td><div class="nd_giohang" align="center"><?php if(isset($product['size'])) echo $product['size'];?></div></td>
                    <td><div class="nd_giohang" align="center"><?php if(isset($product['color'])) echo $product['color'];?></div></td>
                    <td><div class="nd_giohang" align="center">
                    <form name="view<?php echo $i; ?>" action="<?php echo DOMAIN;?>productupdate/<?php echo $key;?>" method="post">
                    <input style="width:50px;" type="text" name="soluong" value="<?php echo $product['sl']; ?>"/>
                    </form>
                    </div></td>
                    <td><div class="nd_giohang" align="center"><?php echo number_format($product['price']*$product['sl']);?> VNĐ</div></td>
                    <td><div class="nd_giohang" align="center"><input onclick="document.view<?php echo $i; ?>.submit();"  type="button" class="btn btn-small" value="Edit" /></div></td>
                    <td><div class="nd_giohang" align="center"><a href="<?php echo DOMAIN;?>productdelete/<?php echo $key;?>"><input type="button" class="btn btn-small" value="Delete" /></a></div></td>
                </tr>
                <?php $total +=$product['total']; $i++;}?>
            </table>
            <div class="" style="margin-top: 8px; text-align: right; margin-right: 4px;"><div class="tongtien_tt"><?php echo lang('tongtien');?>:&nbsp;<span style="color: red; font-weight: bold;"><?php echo number_format($total);?> VNĐ</span></div></div>
        </div>
        
        <div class="box-dieuhuong" style="text-transform: uppercase; font-weight: bold; margin-top: 15px;">
            
        </div>
        <div class="clear-main"></div>
    </div>

</div>

<div class="bg-cat">
    <div class="bg-title-cat bg-title-catff">
        <label>Thông tin khách hàng</label>
    </div>
</div>
<div class="padd-spcat">
    <div class="b-product-list padd-b-product-list">
        <form method="post" id="check_form" action="<?php echo DOMAIN; ?>dat-hang">
        <!--form luu thong tin hang gui mail-->
            <input class="contacts" type="hidden" value="<?php echo $product['images']; ?>" name="images"/>
            <input class="contacts" type="hidden" value="<?php echo $product['name']; ?>" name="product_name"/>
            <input class="contacts" type="hidden" value="<?php echo $product['price']; ?>" name="price"/>
            <input class="contacts" type="hidden" value="<?php echo $product['sl']; ?>" name="sl"/>
            <input class="contacts" type="hidden" value="<?php echo ($product['price']*$product['sl']);?>" name="tt_1sp"/>
        <!---->
            <table class="guimail">
                <tr><td><div class="title_thongtin"><?php echo lang('hoten');?>: </div></td><td><input id="input" placeholder="Name" name="name" class="validate[required]" type="text" required="" /></td></tr>
                <tr><td><div class="title_thongtin"><?php echo lang('phone');?>: </div></td><td><input id="input" placeholder="Phone" type="text" class="validate[required]" name="phone" required="" /></td></tr>
                <tr><td><div class="title_thongtin"><?php echo lang('Email');?>: </div></td><td><input id="email" type="text" placeholder="Email" class="validate[required]" name="email" required="" /></td></tr>
                <tr><td><div class="title_thongtin"><?php echo lang('diachi');?>: </div></td><td><input id="input" placeholder="Address" type="text" class="validate[required]" name="address" style="width: 500px;" required="" /></td></tr>
                <tr><td><div class="title_thongtin"><?php echo 'Yêu cầu thêm';?>: &nbsp; &nbsp;</div></td><td><textarea name="dateto" cols="150" style="width: 500px;" rows="5"></textarea><div style="height: 5px;"></div></td></tr>
                <tr>
                    <td><div class="title_thongtin"><?php echo 'Captcha';?>: &nbsp; &nbsp;</div></td>
                    <td>
                        <input type="text" name="captcha" class="span2" placeholder="captcha" required="" required=""/><img id="img_CAPTCHA_RESULT_send" style="width: 88px;" src="<?php echo DOMAIN;?>captcha" alt="" noloaderror="1" /><img id="reloadCaptcha" class="reloadCapcha" onmouseover="this.style.cursor='pointer'" onclick="img_reload();" title="Đổi mã an toàn" alt="renew capcha" src="<?php echo DOMAIN;?>images/icon-reload.png" />
                    </td>
                </tr>
                <tr><td></td><td><input type="submit" class="btn btn-small btn-danger" value=" <?php echo 'Đặt hàng';?> " style="width: 80px; height: 28px;"/>&nbsp;<input type="reset" class="btn btn-small" value=" <?php echo 'Làm lại';?> " style="width: 80px; height: 28px;"/></td></tr>
            </table>
        </form>
        <script type="text/javascript">
            function img_reload(){
                document.getElementById("img_CAPTCHA_RESULT_send").src = document.getElementById("img_CAPTCHA_RESULT_send").src.split("?")[0] + "?"+Math.random();;
            }
        </script> 
        <div class="clear-main"></div>
    </div>

</div>