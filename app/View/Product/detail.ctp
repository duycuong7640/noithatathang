<div class="bg-cat">
    <div class="bg-title-cat bg-title-catff">
        <h1><?php echo $detailNews['Product']['name']; ?></h1>
    </div>
</div>
<div class="padd-spcat">
    <div class="b-product-list padd-b-product-list">
        <div class="row-fluid">
            <div class="span5">
                <div class="border text-left">
                    <div style="height: 300px; margin-bottom: 15px;">
                        <img id="img_01" src="<?php echo DOMAIN;?>img/h300/fill!<?php echo $detailNews['Product']['images']; ?>" data-zoom-image="<?php echo DOMAIN;?>img/h1000/<?php echo $detailNews['Product']['images']; ?>" title="<?php echo $detailNews['Product']['name']; ?>" alt="<?php echo $detailNews['Product']['name']; ?>"/>
                    </div>
                    <div id="gallery_01"> 
                        <a class="active" href="#" data-image="<?php echo DOMAIN;?>img/h300/fill!<?php echo $detailNews['Product']['images']; ?>" data-zoom-image="<?php echo DOMAIN;?>img/h1000/<?php echo $detailNews['Product']['images']; ?>" title="<?php echo $detailNews['Product']['name']; ?>">
                            <img id="img_01" src="<?php echo DOMAIN;?>img/w55/h55/fill!<?php echo $detailNews['Product']['images']; ?>" title="<?php echo $detailNews['Product']['name']; ?>" alt="<?php echo $detailNews['Product']['name']; ?>"/>
                        </a>
                        <?php
                            $dem = 1;
                            if($detailNews['Product']['multiple']){
                            $img_multi = explode(",",  $detailNews['Product']['multiple']);
                            foreach ($img_multi as $k => $values) {
                            if($values){
                        ?>
                            <a href="#" data-image="<?php echo DOMAIN;?>img/h300/fill!<?php echo $values; ?>" data-zoom-image="<?php echo DOMAIN;?>img/h1000/<?php echo $values; ?>" title="<?php echo $detailNews['Product']['name']; ?>">
                                <img id="img_01" src="<?php echo DOMAIN;?>img/w55/h55/fill!<?php echo $values; ?>" title="<?php echo $detailNews['Product']['name']; ?>" alt="<?php echo $detailNews['Product']['name']; ?>"/>
                            </a>
                        <?php $dem ++;}}}?>
                    </div>
                </div><!--end padding-detail-left-->

                <div class="position-relative">
                    <div class="clear-content"></div><div class="clear-content"></div>
                    <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
                    <g:plusone></g:plusone>
                    <div class="position-absolute" style="margin-top: -21px; left: 85px;">
                        <div class="fb-like" data-href="<?php echo DOMAIN.$detailNews['Product']['link'];?>.html" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
                    </div>
                </div>

            </div><!--end span5-->
            <div class="span7">
                <div class="box-detail-product">
                    <h1 class="title"><?php echo $detailNews['Product']['name']; ?></h1>
                    <div class="box-info-product" style="border: 0;">
                        <div class="row-fluid">
                            <div class="span3">Giá</div>
                            <div class="span9">
                                <span class="giagia">
                                    <?php 
                                        if($detailNews['Product']['price']){
                                            if(is_numeric($detailNews['Product']['price'])){
                                                echo number_format($detailNews['Product']['price'], '0').' <sup>vnđ</sup>';
                                            }else{
                                                echo $detailNews['Product']['price'].' <sup>vnđ</sup>';
                                            }
                                        }else{
                                            echo 'Liên hệ';
                                        }
                                    ?>
                                </span>
                            </div>
                        </div>
                    </div><!--end box-info-product-->
                    <div class="box-info-product">
                        <div class="row-fluid">
                            <div class="span3">Mã</div>
                            <div class="span9"><?php echo $detailNews['Product']['code']; ?></div>
                        </div>
                    </div><!--end box-info-product-->
                    <div class="box-info-product">
                        <div class="row-fluid">
                            <div class="span3">Tình trạng</div>
                            <div class="span9"><?php if($detailNews['Product']['tinhtrang']) echo 'Còn hàng'; else echo 'Hết hàng';?></div>
                        </div>
                    </div><!--end box-info-product-->
                    <form method="post" action="<?php echo DOMAIN;?>mua-hang/<?php echo $detailNews['Product']['id']; ?>">
                        <div class="box-info-product">
                            <div class="row-fluid">
                                <div class="span3">Số lượng</div>
                                <div class="span9">
                                    <div class="row-fluid">
                                        <input type="number" name="soluong" class="span3" value="1" />
                                        <input type="submit" class="btn btn-danger btn-small" value="Mua ngay" />
                                    </div>
                                </div>
                            </div>
                        </div><!--end box-info-product-->
                    </form>
                    <div class="box-info-product">
                        <div class="row-fluid">
                            <div class="span12 b-detail-if"><?php echo $detailNews['Product']['shortdes'];?></div>
                        </div>
                    </div><!--end box-info-product-->
                </div>
            </div><!--end span7-->
        </div>
        <div class="clear-main"></div>
    </div>

</div>

<div class="khoangcach-detal-product"></div>

<div class="hienmb" style="display: none; margin-bottom: 30px;"><?php echo $this->element('formdk');?></div>

<div class="row-fluid">
    <div class="span12">
        <div class="box-tab-content-detail">
            <ul id="myTab" class="nav nav-tabs">
                <li class="active"><a href="#home" data-toggle="tab">Nội dung</a></li>
                <!--<li><a href="#homevd" data-toggle="tab">Video</a></li>-->
                <li class=""><a href="#profile2" data-toggle="tab">Bình luận</a></li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade active in" id="home">
                    <div class="">
                        <div class="ct-tt"><?php echo $detailNews['Product']['content'];?></div>
                    </div><!--end anhien-->
                </div>
                <!--<div class="tab-pane fade" id="homevd">
                    <div class="">
                        <div class="ct-tt"><?php //echo $detailNews['Product']['video'];?></div>
                    </div>
                </div>-->
                <div class="tab-pane fade" id="profile2">
                    <div class="box-comment">
                        
                        <fb:comments href="<?php echo DOMAIN.$detailNews['Product']['link'];?>.html" numposts="5" width="100%"></fb:comments>
                    </div><!--end box-comment-->    
                </div>
            </div>
        </div><!--end box-tab-content-detail-->
        <div class="clear-content"></div><div class="clear-content"></div><div class="clear-content"></div>
    </div><!--end span12-->
</div>

<div class="khoangcach-detal-product"></div>

<div class="bg-cat">
    <div class="bg-title-cat bg-title-catff">
        <label>Sản phẩm liên quan</label>
    </div>
</div>
<div class="padd-spcat">
    <div class="b-product-list padd-b-product-list">
        <ul class="lsp">
            <?php foreach($tinlienquan as $value){?>
            <li>
                <div class="b-threat-product">
                    <div class="b-img-product">
                        <a href="<?php echo DOMAIN.$value['Product']['link'];?>.html" title="<?php echo $value['Product']['name'];?>">
                            <img src="<?php echo DOMAIN;?>img/w485/<?php echo $value['Product']['images'];?>" title="<?php echo $value['Product']['name'];?>" alt="<?php echo $value['Product']['name'];?>" />
                        </a>
                    </div>
                    <a href="<?php echo DOMAIN.$value['Product']['link'];?>.html" title="<?php echo $value['Product']['name'];?>">
                        <h3><?php echo $value['Product']['name'];?></h3>
                    </a>
                    <div class="b-gia">
                        Giá:
                        <span>
                            <?php 
                                if($value['Product']['price']){
                                    if(is_numeric($value['Product']['price'])){
                                        echo number_format($value['Product']['price'], '0').' <sup>vnđ</sup>';
                                    }else{
                                        echo $value['Product']['price'].' <sup>vnđ</sup>';
                                    }
                                }else{
                                    echo 'Liên hệ';
                                }
                            ?>
                        </span>
                    </div>
                </div>
            </li>
            <?php }?>
        </ul>
        <div class="clear-main"></div>
    </div>

</div>

<div class="clear-main"></div>
<?php if($this->Paginator->numbers()){?>
    <div class="pagination">
        <?php
            echo $this->Paginator->first('« Đầu');     
            echo $this->Paginator->prev('« Trước', null, null, array('class' => 'disabled')); 
            echo $this->Paginator->numbers(
                array(
                    'before' => null,
                    'after' => null,
                    
                    'tag' => 'span',
                    'class' => 'number',
                    'modulus' => '6',
                    'separator' => null,
                    'first' => 2,
                    'last' => 2,
                    'ellipsis' => '...',
                    'currentClass' => 'current',
                    'currentTag' => null
                )
            );
            echo $this->Paginator->next('Tiếp »'); 
            echo $this->Paginator->last('Cuối »'); 
            echo $this->Paginator->counter('Trang {:page}/{:pages}. Đang xem {:current}/{:count}.');
        ?>
    </div>
<?php }?>
<div class="clear-main"></div>

<?php 
    /*$mbm = 0;
    $user_agent = strtolower($_SERVER['HTTP_USER_AGENT']);
    if (preg_match("/phone|iphone|itouch|ipod|ipad|symbian|android|htc_|htc-|palmos|blackberry|opera mini|iemobile|windows ce|nokia|fennec|hiptop|kindle|mot |mot-|webos\/|samsung|sonyericsson|^sie-|nintendo/", $user_agent)){
        $mbm = 1;
    }*/
?>
<?php //if(!$mbm){?>
<link href="<?php echo DOMAIN;?>ImageZoom/jquery.fancybox.css" rel="stylesheet" type="text/css" />  
<script src='http://code.jquery.com/jquery-1.9.1.min.js'></script>
<script src='<?php echo DOMAIN;?>ImageZoom/jquery.elevateZoom-3.0.8.min.js'></script>
<script src='<?php echo DOMAIN;?>ImageZoom/jquery.fancybox.js'></script>
<script>
    $("#img_01").elevateZoom({gallery:'gallery_01', cursor: 'pointer', galleryActiveClass: 'active', scrollZoom : true}); 
    
    //pass the images to Fancybox
    $("#img_01").bind("click", function(e) {  
      var ez =   $('#img_01').data('elevateZoom');  
        $.fancybox(ez.getGalleryList());
      return false;
    });
</script>
<?php //}?>
<style type="text/css">
    #gallery_01 img{border:1px solid #ccc; opacity: 0.6; margin-bottom: 3px;}
    #gallery_01 a.active img{border:1px solid #ccc; opacity: 1;}
    #gallery_01 .active img{border:1px solid #333 !important;}
</style>