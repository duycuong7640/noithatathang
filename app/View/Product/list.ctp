<div class="bg-cat bg-cat-fix">
    <div class="bg-title-cat bg-title-catff">
        <h1><?php if(isset($cat['Catproduct'])) echo $cat['Catproduct']['name']; else echo $title_for_layout;?></h1>
    </div>
</div>
<div class="padd-spcat">
    <div class="b-product-list padd-b-product-list">
        <ul class="lsp">
            <?php foreach($listnews as $value){?>
            <li>
                <div class="b-threat-product">
                    <div class="b-img-product">
                        <a href="<?php echo DOMAIN.$value['Product']['link'];?>.html" title="<?php echo $value['Product']['name'];?>">
                            <img src="<?php echo DOMAIN;?>img/w485/fill!<?php echo $value['Product']['images'];?>" title="<?php echo $value['Product']['name'];?>" alt="<?php echo $value['Product']['name'];?>" />
                        </a>
                    </div>
                    <a href="<?php echo DOMAIN.$value['Product']['link'];?>.html" title="<?php echo $value['Product']['name'];?>">
                        <h3><?php echo $value['Product']['name'];?></h3>
                    </a>
                    <div class="b-gia">
                        Giá:
                        <span>
                            <?php 
                                if($value['Product']['price']){
                                    if(is_numeric($value['Product']['price'])){
                                        echo number_format($value['Product']['price'], '0').' <sup>vnđ</sup>';
                                    }else{
                                        echo $value['Product']['price'].' <sup>vnđ</sup>';
                                    }
                                }else{
                                    echo 'Liên hệ';
                                }
                            ?>
                        </span>
                    </div>
                </div>
            </li>
            <?php }?>
        </ul>
        <div class="clear-main"></div>
    </div>

</div>

<div class="clear-main"></div>
<?php if($this->Paginator->numbers()){?>
    <div class="pagination">
        <?php
            echo $this->Paginator->first('« Đầu');     
            echo $this->Paginator->prev('« Trước', null, null, array('class' => 'disabled')); 
            echo $this->Paginator->numbers(
                array(
                    'before' => null,
                    'after' => null,
                    
        			'tag' => 'span',
                    'class' => 'number',
        			'modulus' => '6',
                    'separator' => null,
                    'first' => 2,
                    'last' => 2,
                    'ellipsis' => '...',
        			'currentClass' => 'current',
                    'currentTag' => null
        		)
            );
            echo $this->Paginator->next('Tiếp »'); 
            echo $this->Paginator->last('Cuối »'); 
            echo $this->Paginator->counter('Trang {:page}/{:pages}. Đang xem {:current}/{:count}.');
        ?>
    </div>
<?php }?>
<div class="clear-main"></div>