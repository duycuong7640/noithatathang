<?php foreach ($list_thietke["one"] as $value) { ?>
    <div class="wrap-list">
        <div class="b-list-cat">
            <h2 class="title-cat">
                <a href="<?php echo DOMAIN . $value["link"] ?>"
                   title="<?php echo $value["name"] ?>"><?php echo $value["name"] ?></a>
            </h2>
        </div>
        <div class="b-list-ex1">
            <ul>
                <?php
                foreach ($value["list"] as $row) {
                    $row = $row["Catproduct"];
                    ?>
                    <li>
                        <div class="b-row">
                            <a href="<?php echo DOMAIN . $row["link"] ?>"
                               title="<?php echo $row["name"] ?>">
                                <img src="<?php echo DOMAIN; ?>img/w350/h400/fill!<?php echo $row['images']; ?>"
                                     title="<?php echo $row["name"] ?>" alt="<?php echo $row["name"] ?>"/>
                            </a>
                            <h3>
                                <a href="<?php echo DOMAIN . $row["link"] ?>"
                                   title="<?php echo $row["name"] ?>"><?php echo $row["name"] ?></a>
                            </h3>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
<?php } ?>

<?php if (!empty($list_thietke["two"])) { ?>
    <div class="wrap-list-t">
        <ul class="b-list-ex2">
            <?php foreach ($list_thietke["two"] as $value) { ?>
                <li>
                    <div>
                        <div class="b-row">
                            <a href="<?php echo DOMAIN . $row["link"] ?>"
                               title="<?php echo $row["name"] ?>">
                                <img src="<?php echo DOMAIN; ?>img/w350/h200/fill!<?php echo $row['images']; ?>"
                                     title="<?php echo $row["name"] ?>" alt="<?php echo $row["name"] ?>"/>
                            </a>
                            <h2>
                                <a href="<?php echo DOMAIN . $value["link"] ?>"
                                   title="<?php echo $value["name"] ?>"><?php echo $value["name"] ?></a>
                            </h2>
                            <p><?php echo shortDesc($value["content"], 100); ?></p>
                        </div>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
<?php } ?>