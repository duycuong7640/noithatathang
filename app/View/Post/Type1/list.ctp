<?php if (!empty($listnews)) { ?>
    <div class="wrap-filter wrap-filter-cat">
        <ul>
            <li>
                <a href="#">Danh mục:</a>
            </li>
            <?php
            $url = "";
            if (!empty($_GET["q"]) && !empty($_GET["k"])) {
                $url = "?q=" . $_GET["q"] . "&k=" . $_GET["k"];
            } elseif (!empty($_GET["q"])) {
                $url = "?q=" . $_GET["q"];
            } elseif (!empty($_GET["k"])) {
                $url = "?k=" . $_GET["k"];
            }
            ?>
            <?php foreach ($listnews as $val) { ?>
                <li>
                    <a href="<?php echo DOMAIN . $val["link"] . $url ?>" title="<?php echo $val["name"] ?>" class="<?php echo (($val["id"] == $cat['Catproduct']['id']) ? "active" : "") ?>">
                        <?php echo $val["name"] ?>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </div>
<?php } ?>
<div class="bg-cat">
    <div class="bg-title-cat">
        <h1><?php echo $cat['Catproduct']['name']; ?></h1>
    </div>
</div>
<div class="wrap-filter">
    <ul>
        <li>
            <a href="#">Loại nhà:</a>
        </li>
        <?php foreach ($dataFilter["loainha"] as $k => $row) { ?>
            <li>
                <a href="?q=<?php echo $k; ?><?php echo !empty($_GET["k"]) ? "&k=" . $_GET["k"] : "" ?>"
                   title="<?php echo $row; ?>"
                   class="<?php echo !empty($_GET["q"]) ? (($_GET["q"] == $k) ? "active" : "") : "" ?>">
                    <?php echo $row; ?>
                </a>
            </li>
        <?php } ?>
    </ul>
</div>
<div class="wrap-filter">
    <ul>
        <li>
            <a href="#">Phong cách:</a>
        </li>
        <?php foreach ($dataFilter["phongcach"] as $k => $row) { ?>
            <li>
                <a href="?k=<?php echo $k; ?><?php echo !empty($_GET["q"]) ? "&q=" . $_GET["q"] : "" ?>"
                   title="<?php echo $row; ?>"
                   class="<?php echo !empty($_GET["k"]) ? (($_GET["k"] == $k) ? "active" : "") : "" ?>">
                    <?php echo $row; ?>
                </a>
            </li>
        <?php } ?>
    </ul>
</div>

<div class="b-list-ex3">
    <ul>
        <?php
        foreach ($listposts as $row) {
            $row = $row["Post"];
            ?>
            <li>
                <div class="b-row">
                    <a href="<?php echo DOMAIN . $row["link"] ?>.html"
                       title="<?php echo $row["name"] ?>">
                        <img src="<?php echo DOMAIN; ?>img/w350/h400/fill!<?php echo $row['images']; ?>"
                             title="<?php echo $row["name"] ?>" alt="<?php echo $row["name"] ?>"/>
                    </a>
                    <h3>
                        <a href="<?php echo DOMAIN . $row["link"] ?>.html"
                           title="<?php echo $row["name"] ?>"><?php echo $row["name"] ?></a>
                    </h3>
                </div>
            </li>
        <?php } ?>
    </ul>
</div>
<div class="clear-main"></div>
<?php if ($this->Paginator->numbers()) { ?>
    <div class="pagination">
        <?php
        echo $this->Paginator->first('« Đầu');
        echo $this->Paginator->prev('« Trước', null, null, array('class' => 'disabled'));
        echo $this->Paginator->numbers(
            array(
                'before' => null,
                'after' => null,

                'tag' => 'span',
                'class' => 'number',
                'modulus' => '6',
                'separator' => null,
                'first' => 2,
                'last' => 2,
                'ellipsis' => '...',
                'currentClass' => 'current',
                'currentTag' => null
            )
        );
        echo $this->Paginator->next('Tiếp »');
        echo $this->Paginator->last('Cuối »');
        echo $this->Paginator->counter('Trang {:page}/{:pages}. Đang xem {:current}/{:count}.');
        ?>
    </div>
<?php } ?>
<div class="clear-main"></div>