<div class="bg-cat">
    <div class="bg-title-cat">
        <h1><?php echo $cat['Catproduct']['name']; ?></h1>
    </div>
</div>
<div class="wrap-filter">
    <ul>
        <li>
            <a href="#">Khu vực:</a>
        </li>
        <?php foreach ($dataFilter["khuvuc"] as $k => $row) { ?>
            <li>
                <a href="?kv=<?php echo $k; ?>"
                   title="<?php echo $row; ?>"
                   class="<?php echo !empty($_GET["kv"]) ? (($_GET["kv"] == $k) ? "active" : "") : "" ?>">
                    <?php echo $row; ?>
                </a>
            </li>
        <?php } ?>
    </ul>
</div>

<div class="b-list-ex4">
    <ul>
        <?php
        foreach ($listposts as $row) {
            $row = $row["Post"];
            ?>
            <li>
                <div class="b-row">
                    <a href="<?php echo DOMAIN . $row["link"] ?>.html"
                       title="<?php echo $row["name"] ?>">
                        <img src="<?php echo DOMAIN; ?>img/w350/h400/fill!<?php echo $row['images']; ?>"
                             title="<?php echo $row["name"] ?>" alt="<?php echo $row["name"] ?>"/>
                    </a>
                    <h3>
                        <a href="<?php echo DOMAIN . $row["link"] ?>.html"
                           title="<?php echo $row["name"] ?>"><?php echo $row["name"] ?></a>
                    </h3>
                    <div class="short-content"><?php echo $row["shortdes"] ?></div>
                </div>
            </li>
        <?php } ?>
    </ul>
</div>
<div class="clear-main"></div>
<?php if($this->Paginator->numbers()){?>
    <div class="pagination">
        <?php
        echo $this->Paginator->first('« Đầu');
        echo $this->Paginator->prev('« Trước', null, null, array('class' => 'disabled'));
        echo $this->Paginator->numbers(
            array(
                'before' => null,
                'after' => null,

                'tag' => 'span',
                'class' => 'number',
                'modulus' => '6',
                'separator' => null,
                'first' => 2,
                'last' => 2,
                'ellipsis' => '...',
                'currentClass' => 'current',
                'currentTag' => null
            )
        );
        echo $this->Paginator->next('Tiếp »');
        echo $this->Paginator->last('Cuối »');
        echo $this->Paginator->counter('Trang {:page}/{:pages}. Đang xem {:current}/{:count}.');
        ?>
    </div>
<?php }?>
<div class="clear-main"></div>