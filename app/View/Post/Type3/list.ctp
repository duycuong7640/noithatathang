<div class="bg-cat">
    <div class="bg-title-cat">
        <h1><?php echo $cat['Catproduct']['name']; ?></h1>
    </div>
</div>

<div class="b-list-ex3">
    <ul>
        <?php
        foreach ($listCat as $row) {
            $row = $row["Catproduct"];
            ?>
            <li>
                <div class="b-row">
                    <a href="<?php echo DOMAIN . $row["link"] ?>"
                       title="<?php echo $row["name"] ?>">
                        <img src="<?php echo DOMAIN; ?>img/w350/h280/fill!<?php echo $row['images']; ?>"
                             title="<?php echo $row["name"] ?>" alt="<?php echo $row["name"] ?>"/>
                    </a>
                    <h3>
                        <a href="<?php echo DOMAIN . $row["link"] ?>"
                           title="<?php echo $row["name"] ?>"><?php echo $row["name"] ?></a>
                    </h3>
                </div>
            </li>
        <?php } ?>
    </ul>
</div>