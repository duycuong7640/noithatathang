<?php if(!empty($listnews)){?>
    <div class="wrap-filter wrap-filter-cat">
        <ul>
            <li>
                <a href="#">Danh mục:</a>
            </li>
            <?php foreach ($listnews as $val){?>
                <li>
                    <a href="" title="">
                        <?php echo $val["name"]?>
                    </a>
                </li>
            <?php }?>
        </ul>
    </div>
<?php }?>
<div class="bg-cat">
    <div class="bg-title-cat">
        <h1><?php echo $cat['Catproduct']['name']; ?></h1>
    </div>
</div>
<div class="wrap-filter">
    <ul>
        <li>
            <a href="#">Thương hiệu:</a>
        </li>
        <?php foreach ($dataFilter["thuonghieu"] as $k => $row) { ?>
            <li>
                <a href="?t=<?php echo $k; ?><?php echo !empty($_GET["c"]) ? "&c=" . $_GET["c"] : "" ?>"
                   title="<?php echo $row; ?>"
                   class="<?php echo !empty($_GET["t"]) ? (($_GET["t"] == $k) ? "active" : "") : "" ?>">
                    <?php echo $row; ?>
                </a>
            </li>
        <?php } ?>
    </ul>
</div>
<div class="wrap-filter">
    <ul>
        <li>
            <a href="#">Chủng loại:</a>
        </li>
        <?php foreach ($dataFilter["chungloai"] as $k => $row) { ?>
            <li>
                <a href="?c=<?php echo $k; ?><?php echo !empty($_GET["t"]) ? "&t=" . $_GET["t"] : "" ?>"
                   title="<?php echo $row; ?>"
                   class="<?php echo !empty($_GET["c"]) ? (($_GET["c"] == $k) ? "active" : "") : "" ?>">
                    <?php echo $row; ?>
                </a>
            </li>
        <?php } ?>
    </ul>
</div>

<div class="b-list-ex3">
    <ul>
        <?php
        foreach ($listposts as $row) {
            $row = $row["Post"];
            ?>
            <li>
                <div class="b-row">
                    <a href="<?php echo DOMAIN . $row["link"] ?>.html"
                       title="<?php echo $row["name"] ?>">
                        <img src="<?php echo DOMAIN; ?>img/w350/h400/fill!<?php echo $row['images']; ?>"
                             title="<?php echo $row["name"] ?>" alt="<?php echo $row["name"] ?>"/>
                    </a>
                    <h3>
                        <a href="<?php echo DOMAIN . $row["link"] ?>.html"
                           title="<?php echo $row["name"] ?>"><?php echo $row["name"] ?></a>
                    </h3>
                </div>
            </li>
        <?php } ?>
    </ul>
</div>
<div class="clear-main"></div>
<?php if($this->Paginator->numbers()){?>
    <div class="pagination">
        <?php
        echo $this->Paginator->first('« Đầu');
        echo $this->Paginator->prev('« Trước', null, null, array('class' => 'disabled'));
        echo $this->Paginator->numbers(
            array(
                'before' => null,
                'after' => null,

                'tag' => 'span',
                'class' => 'number',
                'modulus' => '6',
                'separator' => null,
                'first' => 2,
                'last' => 2,
                'ellipsis' => '...',
                'currentClass' => 'current',
                'currentTag' => null
            )
        );
        echo $this->Paginator->next('Tiếp »');
        echo $this->Paginator->last('Cuối »');
        echo $this->Paginator->counter('Trang {:page}/{:pages}. Đang xem {:current}/{:count}.');
        ?>
    </div>
<?php }?>
<div class="clear-main"></div>