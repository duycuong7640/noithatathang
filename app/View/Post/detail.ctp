<div class="bg-cat">
    <div class="bg-title-cat">
        <h1><?php echo $detailNews['Post']['name']?></h1>
    </div>
</div>

<div class="padd-detail">
    <div class="box-content">
        <div class="box-detail-new">
            
            <div class="ct-tt">
                <?php echo $detailNews['Post']['content'];?>
            </div><!--end ct-tt-->
            <div class="time-date">
                <?php echo date('h:i:s', strtotime($detailNews['Post']['created']));?>&nbsp;&nbsp;
                <span><?php echo date('d/m/Y', strtotime($detailNews['Post']['created']));?></span>
            </div><!--end time-date--><div class="clear-content"></div>
            <div class="box-like-share">
                <div class="fb-like" data-href="<?php echo DOMAIN.$detailNews['Post']['link'];?>.htm" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
            </div><!--end box-like-share--><div class="clear-content"></div>

        </div><!--end box-new-detail-->

        <div class="hienmb" style="display: none; margin-bottom: 30px;"><?php echo $this->element('formdk');?></div>
        
        <div class="bar-new-detail">
            <label>Tin cùng chuyên mục</label>
            <div class="clear-main"></div>
            <ul>
                <?php foreach($tinlq as $value){?>
                <li>
                    <a href="<?php echo DOMAIN.$value['Post']['link'];?>.htm" title="<?php echo $value['Post']['name'];?>">
                        <h3><?php echo $value['Post']['name'];?> &nbsp;<span>(<?php echo date('d-m-Y h:i:s', strtotime($value['Post']['created'])); ?>)</span></h3>
                    </a>
                </li>
                <?php }?>
            </ul>
            <div class="clear-main"></div>
        </div><!--end bar-new-detail-->
    </div>
</div>