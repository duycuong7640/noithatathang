<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:fb="http://ogp.me/ns/fb#">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link type="image/x-icon" href="<?php echo DOMAIN ?>images/20x15.png" rel="icon">
    <link type="image/x-icon" href="<?php echo DOMAIN ?>images/20x15.png" rel="shortcut icon">
    <?php
    if (isset($keywords_for_layout)) {
        echo "<meta name='keywords' content='" . $keywords_for_layout . "' /> ";
    }
    if (isset($description_for_layout)) {
        echo "<meta name='description' content='" . $description_for_layout . "' />";
    }
    ?>
    <title>
        <?php
        if (isset($title_for_layout)) {
            echo $title_for_layout;
        }
        ?>
    </title>
    <?php
        $css = array(
            '/icon-font/css/font-awesome',
            '/css/root/bootstrap.min',
            '/css/root/pagination',
            '/css/root/notive',
            '/css/menu/ddsmoothmenu',
            '/css/adv',
            '/css/style',
            '/css/reset'
        );
        $js = array(
            '/js/root/jquery-1.7.2.min.js',
            '/js/root/bootstrap.min.js',
            '/js/jquery.js',
            '/js/menu/ddsmoothmenu',
        );
    
        echo $this->Html->css($css);
        echo $this->Html->script($js);
    ?>
    <script type="text/javascript">
        ddsmoothmenu.init({
        	mainmenuid: "smoothmenu1", //menu DIV id
        	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
        	classname: 'navhor', //class added to menu's outer DIV
        	//customtheme: ["#1c5a80", "#18374a"],
        	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
        })
    </script>
</head>
<body>

<?php echo $this->element('header');?>
<?php echo $this->element('menu');?>
<div class="main-content">
    <div class="clear-main"></div>
    <?php echo $this->fetch('content');?>
    <div class="clear-main"></div>
    <?php echo $this->element('footer');?>
</div><!--end main-content-->
</body>
</html>
