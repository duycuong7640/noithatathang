<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:fb="http://ogp.me/ns/fb#" class="no-js" lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link type="image/x-icon" href="<?php echo DOMAIN ?>images/ll2.png" rel="icon">
    <link type="image/x-icon" href="<?php echo DOMAIN ?>images/ll2.png" rel="shortcut icon">
    <?php
    if (isset($keywords_for_layout)) {
        echo "<meta name='keywords' content='" . $keywords_for_layout . "' /> ";
    }
    if (isset($description_for_layout)) {
        echo "<meta name='description' content='" . $description_for_layout . "' />";
    }
    ?>
    <title>
        <?php
        if (isset($title_for_layout)) {
            echo $title_for_layout;
        }
        ?>
    </title>
    <?php
    $css = array(
        '/css/root/bootstrap.min',
        '/css/root/bootstrap-responsive',
        '/icon-font/css/font-awesome',
        '/css/root/pagination',
        '/css/root/notive',
        '/css/menu/ddsmoothmenu',
        //'/chay2ben/fix-menu-chay',
        '/css/animate',
        '/css/style',
        '/css/responsive',
        '/css/reset'
    );
    $js = array(
        '/js/root/jquery-1.7.2.min.js',
        '/js/root/bootstrap.min.js',
        //'/js/root/application',
        //'/js/jquery.min',
        '/js/menu/ddsmoothmenu',
        //'/chay2ben/nagging-menu',
        '/js/wow.min',
    );

    echo $this->Html->css($css);
    echo $this->Html->script($js);
    ?>
    <script type="text/javascript">
        ddsmoothmenu.init({
            mainmenuid: "smoothmenu1", //menu DIV id
            orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
            classname: 'navhor', //class added to menu's outer DIV
            //customtheme: ["#1c5a80", "#18374a"],
            contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
        })
    </script>
    <script>
        new WOW().init();
    </script>

    <style>
        /*body {
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            -o-user-select: none;
            user-select: none;
        }*/
    </style>
    <script type="text/JavaScript">
        // function killCopy(e) {
        //     return false
        // }

        // function reEnable() {
        //     return true
        // }

        // document.onselectstart = new Function(“return false”)
        // if (window.sidebar) {
        //     document.onmousedown = killCopy
        //     document.onclick = reEnable
        // }
    </script>
    <script type="text/javascript">
        //<![CDATA[
        // JavaScript Document
        // var message = "NoRightClicking";

        // function defeatIE() {
        //     if (document.all) {
        //         (message);
        //         return false;
        //     }
        // }

        // function defeatNS(e) {
        //     if (document.layers || (document.getElementById && !document.all)) {
        //         if (e.which == 2 || e.which == 3) {
        //             (message);
        //             return false;
        //         }
        //     }
        // }

        // if (document.layers) {
        //     document.captureEvents(Event.MOUSEDOWN);
        //     document.onmousedown = defeatNS;
        // } else {
        //     document.onmouseup = defeatNS;
        //     document.oncontextmenu = defeatIE;
        // }
        // document.oncontextmenu = new Function("return false")
        //]]>
    </script>

</head>

<body>
<!--<div id="fb-root"></div>-->
<!--<script>(function(d, s, id) {-->
<!--  var js, fjs = d.getElementsByTagName(s)[0];-->
<!--  if (d.getElementById(id)) return;-->
<!--  js = d.createElement(s); js.id = id;-->
<!--  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.0";-->
<!--  fjs.parentNode.insertBefore(js, fjs);-->
<!--}(document, 'script', 'facebook-jssdk'));</script>-->

<div class="chay2ben"><?php //echo $this->element('chay2ben');?></div>
<?php //echo $this->element('header-top');?>
<?php echo $this->element('header');?>
<?php echo $this->element('menu');?>
<?php echo $this->element('dieuhuong');?>
<div class="main-width">
    <div class="b-content-detail">
        <div class="row-fluid">
            <div class="span12">
                <?php echo $this->fetch('content');?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->element('footer');?>

<?php echo $setting['chenthekhac'];?>
</body>
</html>