<div class="wrap-form-dk">
    <div class="main-width">
        <div class="row-fluid">
            <div class="span2">
                <div class="form-ic1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" preserveAspectRatio="none"
                         viewBox="0 0 156.2 150" fill="rgba(255, 255, 255, 1)">
                        <path d="M150.1,12.1,20,142.3a37.1,37.1,0,0,0,5.5,3.6l128-128A30.2,30.2,0,0,0,150.1,12.1Z"></path>
                        <path d="M155.7,25,32.1,148.6a34.2,34.2,0,0,0,7.7,1.4L156.1,33.7v-.6A32,32,0,0,0,155.7,25Z"></path>
                        <path d="M.6,23.9,23.7.8A30.2,30.2,0,0,0,.6,23.9Z"></path>
                        <path d="M140,3.5,11.2,132.4a37,37,0,0,0,4,5.4L145.6,7.3A30.2,30.2,0,0,0,140,3.5Z"></path>
                        <polygon points="76.4 150 153.1 73.3 153.9 63.3 67.2 150 76.4 150"></polygon>
                        <polygon points="94.7 150 151.7 93 152.4 83.2 85.6 150 94.7 150"></polygon>
                        <polygon points="70.4 0 2.5 67.9 3.1 76.3 79.5 0 70.4 0"></polygon>
                        <polygon points="51.7 0 1.2 50.5 1.9 59.4 61.3 0 51.7 0"></polygon>
                        <polygon points="40.5 0 40.5 0 33.8 0 0 33.8 0.6 42 42.6 0 40.5 0"></polygon>
                        <polygon points="88.5 0 3.8 84.8 4.4 93.5 97.9 0 88.5 0"></polygon>
                        <polygon points="114 0 106.9 0 5 101.9 5.7 110.6 116.3 0 114 0"></polygon>
                        <path d="M125.6,0,6.4,119.2a34.2,34.2,0,0,0,1.8,7L133.4,1A32.2,32.2,0,0,0,125.6,0Z"></path>
                        <polygon points="58.1 150 154.6 53.5 155.3 43.5 48.8 150 58.1 150"></polygon>
                        <path d="M148.9,123.1l-25.7,25.7A36.8,36.8,0,0,0,148.9,123.1Z"></path>
                        <polygon points="112.9 150 150.2 112.6 150.9 103.1 104 150 112.9 150"></polygon>
                    </svg>
                </div>
            </div>
            <div class="span7">
                <div class="title-form1">ĐĂNG KÝ KHÁM MIỄN PHÍ</div>
                <form class="" method="post" action="<?php echo DOMAIN;?>dang-ky-tu-van">
                    <div class="row-fluid">
                        <div class="span6">
                            <input type="text" name="name" placeholder="Họ và tên" class="span12 ipf" required="" />
                        </div>
                        <div class="span6">
                            <input type="text" name="phone" placeholder="Nhập Số điện thoại" class="span12 ipf" required="" />
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <input type="text" name="benhly" placeholder="Bệnh lý" class="span12 ipf" />
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <textarea name="content" class="span12 iptext" rows="5" placeholder="Để lại lời nhắn cho chúng tôi"></textarea>
                        </div>
                    </div>
                    <div class="b-smf">
                        <button type="submit" class="fmsmf">Đăng ký</button>
                    </div>
                </form>
            </div>
            <div class="span3">
                <div class="form-ic2 position-relative">
                    <div class="fi2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" preserveAspectRatio="none"
                             viewBox="0 0 150.2 150" fill="rgba(255, 255, 255, 1)">
                            <rect width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect y="24.2" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect y="48.5" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect y="72.7" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="24.2" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="24.2" y="24.2" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="24.2" y="48.5" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="24.2" y="72.7" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="48.5" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="48.5" y="24.2" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="48.5" y="48.5" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="48.5" y="72.7" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="72.7" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="72.7" y="24.2" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="72.7" y="48.5" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="72.7" y="72.7" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="97" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="97" y="24.2" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="97" y="48.5" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="97" y="72.7" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="121.2" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="121.2" y="24.2" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="121.2" y="48.5" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="121.2" y="72.7" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="145.5" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="145.5" y="24.2" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="145.5" y="48.5" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="145.5" y="72.7" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect y="97" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect y="121.2" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect y="145.5" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="24.2" y="97" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="24.2" y="121.2" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="24.2" y="145.5" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="48.5" y="97" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="48.5" y="121.2" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="48.5" y="145.5" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="72.7" y="97" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="72.7" y="121.2" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="72.7" y="145.5" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="97" y="97" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="97" y="121.2" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="97" y="145.5" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="121.2" y="97" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="121.2" y="121.2" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="121.2" y="145.5" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="145.5" y="97" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="145.5" y="121.2" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                            <rect x="145.5" y="145.5" width="4.7" height="4.54" rx="2.2" ry="2.2"></rect>
                        </svg>
                    </div>
                    <div class="fi1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" preserveAspectRatio="none"
                             viewBox="0 0 156.2 150" fill="rgba(255, 255, 255, 1)">
                            <path d="M150.1,12.1,20,142.3a37.1,37.1,0,0,0,5.5,3.6l128-128A30.2,30.2,0,0,0,150.1,12.1Z"></path>
                            <path d="M155.7,25,32.1,148.6a34.2,34.2,0,0,0,7.7,1.4L156.1,33.7v-.6A32,32,0,0,0,155.7,25Z"></path>
                            <path d="M.6,23.9,23.7.8A30.2,30.2,0,0,0,.6,23.9Z"></path>
                            <path d="M140,3.5,11.2,132.4a37,37,0,0,0,4,5.4L145.6,7.3A30.2,30.2,0,0,0,140,3.5Z"></path>
                            <polygon points="76.4 150 153.1 73.3 153.9 63.3 67.2 150 76.4 150"></polygon>
                            <polygon points="94.7 150 151.7 93 152.4 83.2 85.6 150 94.7 150"></polygon>
                            <polygon points="70.4 0 2.5 67.9 3.1 76.3 79.5 0 70.4 0"></polygon>
                            <polygon points="51.7 0 1.2 50.5 1.9 59.4 61.3 0 51.7 0"></polygon>
                            <polygon points="40.5 0 40.5 0 33.8 0 0 33.8 0.6 42 42.6 0 40.5 0"></polygon>
                            <polygon points="88.5 0 3.8 84.8 4.4 93.5 97.9 0 88.5 0"></polygon>
                            <polygon points="114 0 106.9 0 5 101.9 5.7 110.6 116.3 0 114 0"></polygon>
                            <path d="M125.6,0,6.4,119.2a34.2,34.2,0,0,0,1.8,7L133.4,1A32.2,32.2,0,0,0,125.6,0Z"></path>
                            <polygon points="58.1 150 154.6 53.5 155.3 43.5 48.8 150 58.1 150"></polygon>
                            <path d="M148.9,123.1l-25.7,25.7A36.8,36.8,0,0,0,148.9,123.1Z"></path>
                            <polygon points="112.9 150 150.2 112.6 150.9 103.1 104 150 112.9 150"></polygon>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>