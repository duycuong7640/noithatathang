<div class="row-fluid">
    <div class="span8">
        <div class="col-thuonghieu">
            <div class="row-fluid">
                <div class="span6">
                    <label class="title-baovethuonghieu">Bảo vệ thương hiệu ngay với chúng tôi.</label>
                    <div class="content-thuonghieu">
                        Bằng việc thực hiện các bước sau:<br />
                        * Nhập tên miền cần đăng ký;<br />
                        * Lựa chọn loại tên miền * Click nút GO
                    </div>
                    <form class="form-search">
                        <input type="text" name="tenmien" class="input-medium search-query" placeholder="Tên miền" required=""/>
                        <button type="submit" class="btn">Go</button>
                    </form>
                </div>
                <div class="span6">
                    <ul class="checktenmien">
                        <?php $dem = 0; foreach($namespace as $key=>$value){?>
                            <li <?php if($dem % 3 == 0 && $dem != 0) echo 'style="margin-right: 0;"';?>><input type="checkbox" name=""/><?php echo $value;?></li>
                        <?php $dem ++; if($dem == 4) $dem = 0;}?>
                    </ul>
                    <div class="clear-main"></div>
                </div>
            </div>            
        </div><!--end col-thuonghieu-->
    </div>
    <div class="span4 bg">
        <?php if(!empty($khuyenmai)){?>
            <a href="<?php echo $khuyenmai['Extention']['url'];?>" title="<?php echo $khuyenmai['Extention']['name'];?>">
                <img src="<?php echo DOMAIN;?>img/h166/<?php echo $khuyenmai['Extention']['images'];?>"/>
            </a>
        <?php }?>
    </div>
</div>
<div class="clear-content"></div><div class="clear-content"></div>