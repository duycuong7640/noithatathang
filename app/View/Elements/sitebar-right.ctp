<div class="anmb">
	<div class="bg-catright">
		<a href="<?php echo DOMAIN;?>tin-tuc">
			<label>Tin tức</label>
		</a>
	</div>
	<?php foreach($list_tintuchome as $value){?>
		<div class="b-list-newr">
			<div class="row-fluid">
				<div class="span4">
					<a href="<?php echo DOMAIN.$value['Post']['link'];?>.htm" title="<?php echo $value['Post']['name'];?>">
	                    <img class="image" src="<?php echo DOMAIN;?>img/w205/h152/fill!<?php echo $value['Post']['images'];?>" title="<?php echo $value['Post']['name'];?>" alt="<?php echo $value['Post']['name'];?>"/>
	                </a>
				</div>
				<div class="span8">
					<a href="<?php echo DOMAIN.$value['Post']['link'];?>.htm" title="<?php echo $value['Post']['name'];?>">
		                <h3><?php echo $value['Post']['name'];?></h3>
		            </a>
					<div class="b-shortdes"><?php echo shortDesc($value['Post']['shortdes'], 50);?></div>
				</div>
			</div>
			<div class="clear-main"></div>
		</div><!--end b-list-newr-->
	<?php }?>
</div>

<div class="bg-catright">
	<a href="<?php echo DOMAIN;?>video">
		<label>Video</label>
	</a>
</div>
<div class="b-video"><?php echo $setting['video'];?></div>

<div class="bg-catright">
	<label>Kết nối facebook</label>
</div>
<div class="b-faceb">
	<div class="fb-page" data-href="<?php echo $setting['facebook'];?>" data-width="335" data-small-header="false" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="<?php echo $setting['facebook'];?>"><a href="<?php echo $setting['facebook'];?>"><?php echo $setting['facebook'];?></a></blockquote></div></div>
</div>

<?php foreach($quangcaophai as $value){?>
	<div class="b-quangcaophai">
		<a href="<?php echo $value['Extention']['url'];?>" target="_blank">
			<img src="<?php echo DOMAIN.'img/'.$value['Extention']['images']; ?>" title="<?php echo $value['Extention']['name'];?>" alt="<?php echo $value['Extention']['name'];?>" />
		</a>
	</div>
<?php }?>

<div class="hienmb" style="display: none;">
	<div class="bg-catright">
		<a href="<?php echo DOMAIN;?>tin-tuc">
			<label>Tin tức</label>
		</a>
	</div> 
	<?php foreach($list_tintuchome as $value){?>
		<div class="b-list-newr">
			<div class="row-fluid">
				<div class="span4">
					<a href="<?php echo DOMAIN.$value['Post']['link'];?>.htm" title="<?php echo $value['Post']['name'];?>">
	                    <img class="image" src="<?php echo DOMAIN;?>img/w205/h152/fill!<?php echo $value['Post']['images'];?>" title="<?php echo $value['Post']['name'];?>" alt="<?php echo $value['Post']['name'];?>"/>
	                </a>
				</div>
				<div class="span8">
					<a href="<?php echo DOMAIN.$value['Post']['link'];?>.htm" title="<?php echo $value['Post']['name'];?>">
		                <h3><?php echo $value['Post']['name'];?></h3>
		            </a>
					<div class="b-shortdes"><?php echo shortDesc($value['Post']['shortdes'], 50);?></div>
				</div>
			</div>
			<div class="clear-main"></div>
		</div><!--end b-list-newr-->
	<?php }?>
</div>

<div class="bg-catright">
	<label>Thống kê truy cập</label>
</div>
<div class="b-video"><?php echo $this->element('counter');?></div>