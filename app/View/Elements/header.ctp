<?php
$bg = "";
if (!empty($banner)) {
    $bg = DOMAIN . 'img/fill!' . $banner['Extention']['images'];
}
?>
<div class="wrap-head" style="background: url('<?php echo $bg; ?>'); background-size: cover;">
    <div class="bg-head">
        <div class="main-width position-relative">
            <div class="b-search-head">
                <form method="post" action="<?php echo DOMAIN; ?>tim-kiem">
                    <input type="text" name="tukhoa" placeholder="Nhập từ khoá tìm kiếm" class="inputTukhoa"/>
                    <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                </form>
            </div>
            <label>Xây Dựng Nội Ngoại Thất .com</label>
        </div>
    </div>
</div>