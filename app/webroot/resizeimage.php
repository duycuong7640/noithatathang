<?

if(! defined('MAX_WIDTH') ) 			define ('MAX_WIDTH', 1500);
if(! defined('MAX_HEIGHT') ) 			define ('MAX_HEIGHT', 1000);

$p = null;

if(isset($_GET['q'])){
	$str = explode('/',$_GET['q']);
	
	$img = $str[count($str)-1];
	//print_r($img);die;
	unset($str[count($str)-1]);
    
    if(in_array('fill',$str)) $p = 'fill';
    else if(in_array('fit',$str)) $p = 'fit';
    else if(in_array('center',$str)) $p = 'center';
    else if(in_array('centre',$str)) $p = 'center';
	
	$option = array();
	$option['src'] = 'img/'.$img;
	
	foreach($str as $para){
		$option[substr($para,0,1)] = substr($para,1);
	}
}
    
$src = param('src');
$w = param('w',0);
$h = param('h',0);

resize($src,$w,$h,$p);

function param($property, $default = ''){
    global $option;

	if(isset($option[$property])) return $option[$property];
	else if (isset ($_GET[$property])) {
		return $_GET[$property];
	} else {
		return $default;
	}
}

function resize($img = null, $new_width = 0, $new_height = 0,$position = null) {

    if(!is_numeric($new_width)) $new_width = 0;
    if(!is_numeric($new_height)) $new_height = 0;

	//Check if GD extension is loaded
	if (!extension_loaded('gd') && !extension_loaded('gd2')) {
    	trigger_error("GD is not loaded", E_USER_WARNING);
    	return false;
	}

	$no_image = file_exists('../../Config/no-image.png')?'../../Config/no-image.png':null;
    if(!is_file($img) || !file_exists($img)){
		$img = $no_image;
	}
    
	$imgInfo = getimagesize($img);
	$width = $imgInfo[0];
	$height = $imgInfo[1];
	$mime = $imgInfo['mime'];
	
	if($mime == 'image/gif'){
		header('Content-Type: '.$mime);
		$im = imagecreatefromgif($img);
	} else if($mime == 'image/jpeg'){
		header('Content-Type: '.$mime);
		$im = imagecreatefromjpeg($img);
	} else if($mime == 'image/png'){
		header('Content-Type: '.$mime);
		$im = imagecreatefrompng($img);
	} else {
		header('Content-Type: image/png');
		$im = imagecreatefrompng($img);
	}


	if (!$new_width && !$new_height){
		$new_width = $width;
		$new_height = $height;
	} else if ($new_width && !$new_height){
		$new_height = $height*($new_width/$width);
	} else if (!$new_width && $new_height){
		$new_width = $width*($new_height/$height);
	}

	if($new_width > MAX_WIDTH){
		$new_height = $new_height*(MAX_WIDTH/$new_width);
		$new_width = MAX_WIDTH;
	}

	if($new_height > MAX_HEIGHT){
		$new_width = $new_width*(MAX_HEIGHT/$new_height);
		$new_height = MAX_HEIGHT;
	}
/*
	//If image dimension is smaller, do not resize
	if ($width <= $new_width && $height <= $new_height) {
	$new_height = $height;
	$new_width = $width;
	}
*/
	 
	$new_width = round($new_width);
	$new_height = round($new_height);
	 
	$newImg = imagecreatetruecolor($new_width, $new_height);

	if(($mime == 'image/gif') OR ($mime=='image/png')){
		imagealphablending($newImg, false);
		imagesavealpha($newImg,true);
		$transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
		imagefilledrectangle($newImg, 0, 0, $new_width, $new_height, $transparent);
	}
    
    $im_width = $width;
    $im_height = $height;
    $im_x = 0;
    $im_y = 0;
    
    if($position == 'center'){
        $im_width = $new_width;
        $im_height = $new_height;
        $im_x = round(($width-$im_width)/2);
        $im_y = round(($height-$im_height)/2);
    }
    else if($position == 'fill'){
        if($new_width > $width*($new_height/$height)){
            $im_width = $width;
            $im_height = $new_height*($width/$new_width);
            $im_x = 0;
            $im_y = round(($height-$im_height)/2);
        }
        else{
            $im_width = $new_width*($height/$new_height);
            $im_height = $height;
            $im_x = round(($width-$im_width)/2);
            $im_y = 0;
        }
    }
    else if($position == 'fit'){
        if($new_width < $width*($new_height/$height)){
            $im_width = $width;
            $im_height = $new_height*($width/$new_width);
            $im_x = 0;
            $im_y = round(($height-$im_height)/2);
        }
        else{
            $im_width = $new_width*($height/$new_height);
            $im_height = $height;
            $im_x = round(($width-$im_width)/2);
            $im_y = 0;
        }
    }
    
	imagecopyresampled($newImg, $im, 0, 0, $im_x, $im_y, $new_width, $new_height, $im_width, $im_height);
	 
	if($mime == 'image/gif'){
		imagegif($newImg);
	} else if($mime == 'image/jpeg'){
		imagejpeg($newImg);
	} else if($mime == 'image/png'){
		imagepng($newImg);
	} else {
		imagepng($newImg);
	}
	imagedestroy($im);
	imagedestroy($newImg);
}
?>