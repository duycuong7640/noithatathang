<!-- Page Head -->
<!-----------------------------------
 * @author  :   Yêu Tinh
 * @email   :   domain.web.online@gmail.com
 * @since   :   8-07-2013
 ------------------------------------>
 

<ul style="float: right;" class="shortcut-buttons-set">
	
	<li><a class="shortcut-button png48-save" href="javascript:void(0);" onclick="javascript:document.adminForm.submit();"><span class="png_bg">
		Lưu
	</span></a></li>
	
	<li><a class="shortcut-button png48-info" href="#messages"><span class="png_bg">
		Trợ giúp
	</span></a></li>
	
	<li><a class="shortcut-button png48-cancel" href="<?php echo DOMAINAD.strtolower(basename(dirname(__FILE__)));?>"><span class="png_bg">
		Hủy
	</span></a></li>

</ul><!-- End .shortcut-buttons-set -->

<div class="clear"></div> <!-- End .clear -->


<?php echo $this->Session->flash(); ?>

<?php echo $this->Form->create(null, array('url' => DOMAINAD.strtolower(basename(dirname(__FILE__))), 'type' => 'post', 'name' => 'adminForm', 'inputDefaults' => array('label' => false, 'div' => false, 'legend'=> false))); ?>
<?php echo $this->Form->input('Setting.id', array()); ?>
<div class="content-box"><!-- Start Content Box -->
	<div class="content-box-header">
    
    
		<h3>Cấu hình</h3>
        <ul class="content-box-tabs">
            <li><a href="#tab1" class="default-tab">Cấu hình chung</a></li> <!-- href must be unique and match the id of target div -->

        </ul>
        
		<div class="clear"></div>
	</div> <!-- End .content-box-header -->
	<div class="content-box-content">
		<div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->
            
            <table width="100%" class="input">
                <tr>
                    <td width="120" class="label">Tên công ty:</td>
                    <td><?php echo $this->Form->input('Setting.name', array('class' => 'text-input medium-input')); ?></td>
                </tr>
                <tr>
                	<td class="label">Tiêu đề website:</td>
                	<td> <?php echo $this->Form->input('Setting.title', array('class' => 'text-input medium-input')); ?></td>
               	</tr>
                <tr>
                    <td class="label">Email:</td>
                    <td> <?php echo $this->Form->input('Setting.email', array('class' => 'text-input medium-input')); ?> </td>
                </tr>
                <tr>
                    <td class="label">Hotline:</td>
                    <td> <?php echo $this->Form->input('Setting.hotline', array('class' => 'text-input medium-input','maxlength' => '12')); ?> </td>
                </tr>
                <tr>
                    <td class="label">Face:</td>
                    <td> <?php echo $this->Form->input('Setting.facebook', array('class' => 'text-input medium-input')); ?> </td>
                </tr>
                <!--<tr>
                    <td class="label">Hotline 2:</td>
                    <td> <?php echo $this->Form->input('Setting.telephone', array('class' => 'text-input medium-input','maxlength' => '12')); ?> </td>
                </tr>
                <tr>
                    <td class="label">Địa chỉ:</td>
                    <td> <?php echo $this->Form->input('Setting.address', array('class' => 'text-input medium-input')); ?> </td>
                </tr>
                -->
                <!--<tr>
                    <td class="label">Tiwter:</td>
                    <td> <?php //echo $this->Form->input('Setting.tiwter', array('class' => 'text-input medium-input')); ?> </td>
                </tr>
                <tr>
                    <td class="label">V:</td>
                    <td> <?php //echo $this->Form->input('Setting.v', array('class' => 'text-input medium-input')); ?> </td>
                </tr>
                <tr>
                    <td class="label">Gooogle cộng:</td>
                    <td> <?php //echo $this->Form->input('Setting.google', array('class' => 'text-input medium-input')); ?> </td>
                </tr>
                <tr>
                    <td class="label">in:</td>
                    <td> <?php //echo $this->Form->input('Setting.in', array('class' => 'text-input medium-input')); ?> </td>
                </tr>
                <tr>
                    <td class="label">cau:</td>
                    <td> <?php //echo $this->Form->input('Setting.cau', array('class' => 'text-input medium-input')); ?> </td>
                </tr>-->
                <tr>
                    <td class="label">Cấu hình Footer:</td>
                    <td><?php  echo $this->Form->input('Setting.footer',array('class'=>'ckeditor-mini'));?></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="label">Thông tin liên hệ:</td>
                    <td><?php  echo $this->Form->input('Setting.contactinfo',array('class'=>'ckeditor-mini'));?></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="label">Bản đồ:</td>
                    <td>
                        <?php echo $this->Form->input('Setting.googlemap', array('class'=>'text-input large-input')); ?>
                    </td>
                </tr>
                <!--<tr>
                    <td class="label">Thông tin hỗ trợ footer:</td>
                    <td><?php  echo $this->Form->input('Setting.thongtinhotro',array('class'=>'ckeditor'));?></td>
                    <td></td>
                </tr>-->
                <tr>
                    <td class="label">Video</td>
                    <td>
                        <?php echo $this->Form->input('Setting.video', array('class'=>'text-input large-input')); ?>
                    </td>
                </tr>
                <tr>
                    <td class="label">Thẻ h1 trang chủ</td>
                    <td>
                        <?php echo $this->Form->input('Setting.theh1', array('class'=>'text-input large-input')); ?>
                    </td>
                </tr>
                <tr>
                    <td class="label">Thẻ link khác (subiz.../ google anlantic / adv):</td>
                    <td>
                        <?php echo $this->Form->input('Setting.chenthekhac', array('class'=>'text-input large-input')); ?>
                    </td>
                </tr>
                <tr>
                    <td class="label">Từ khóa (SEO):</td>
                    <td>
                        <?php echo $this->Form->input('Setting.meta_key', array('class'=>'text-input large-input')); ?>
                    </td>
                </tr>
                <tr>
                    <td class="label">Mô tả (SEO):</td>
                    <td>
                        <?php echo $this->Form->input('Setting.meta_des', array('class'=>'text-input large-input')); ?>
                    </td>
                </tr>
            </table>
            
            <div class="clear"></div>
		</div> <!-- End #tab1 -->
       
	</div> <!-- End .content-box-content -->
</div> <!-- End .content-box -->
<?php echo $this->Form->end(); ?>



<ul style="float: right;" class="shortcut-buttons-set">
	
	<li><a class="shortcut-button png48-save" href="javascript:void(0);" onclick="javascript:document.adminForm.submit();"><span class="png_bg">
		Lưu
	</span></a></li>
	
	<li><a class="shortcut-button png48-info" href="#messages"><span class="png_bg">
		Trợ giúp
	</span></a></li>
	
	<li><a class="shortcut-button png48-cancel" href="<?php echo DOMAINAD.strtolower(basename(dirname(__FILE__)));?>"><span class="png_bg">
		Hủy
	</span></a></li>

</ul><!-- End .shortcut-buttons-set -->

<div class="clear"></div>