<?php


/** ******************************
 * @author  :   Yêu Tinh
 * @email   :   domain.web.online@gmail.com
 * @since   :   8-07-2013
 *********************************/
 
class OrdersController extends AppController {

    public $name = 'Orders';
    public $uses = array('Order');
    
    public function beforeFilter() {
        parent::beforeFilter();
        if(in_array($this->request->params['action'],array('index','search'))) $this->save_url();
    }

    public function index() {
        $this->paginate = array(
            'conditions' => array(
                //'Order.type' => 'vip'
            ),
            'Order' => array('Order.id' => 'DESC'),
            'limit' => '20'
        );
        
        $this->set('view', $this->paginate('Order',array()));	
    }
    
    public function delete($id = null) {
        $delete = $this->Order->findById($id);
        if (!$delete) {
            if(isset($this->notice['not_exist'])) $this->Session->setFlash($this->notice['not_exist'], 'default', array('class' => 'notification error png_bg'));
            $this->cancel();
        }
        else{
            $this->Order->delete($id);
            if(isset($this->notice['delete_success'])) $this->Session->setFlash($this->notice['delete_success'], 'default', array('class' => 'notification success png_bg'));
            $this->cancel();
        }
    }
    
    public function close($id = null) {
        $this->Order->id = $id;
        $this->Order->saveField('vip', 'no');
        if(isset($this->notice['close'])) $this->Session->setFlash($this->notice['close'], 'default', array('class' => 'notification success png_bg'));
        $this->cancel();
    }

    public function active($id = null) {
        $this->Order->id = $id;
        $this->Order->saveField('vip', 'yes');
        if(isset($this->notice['active'])) $this->Session->setFlash($this->notice['active'], 'default', array('class' => 'notification success png_bg'));
        $this->cancel();
    }
}
